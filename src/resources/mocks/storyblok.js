export const fetchStory = {
  name: 'post title',
  created_at: '2020-04-19T01:10:19.611Z',
  published_at: '2020-04-20T05:55:31.978Z',
  tag_list: ['cannabis', 'cbd'],
  content: {
    title: 'Best cannabis strains for anxiety',
    subTitle: 'this is the sub-title - woohoo!',
    seoDescription:
      'Read our guide to CBD, and learn some general CBD terminolog',
    component: 'post',
    mainImage:
      '//a.storyblok.com/f/81752/5000x3334/61ff955732/add-weed-bu6bsersl_m-unsplash.jpg',
    body: {
      type: 'doc',
      content: [
        {
          type: 'paragraph',
          content: [
            {
              text:
                'In the last article of our guide to CBD, you learned some general CBD terminolog' +
                'y that is often misunderstood, the three main types of CBD oil and the differenc' +
                'e between an oil and a tincture. Now that you know about the types of CBD concen' +
                'trates, you’ll learn the science behind CBD oil and its uses.',
              type: 'text',
            },
          ],
        },
        {
          type: 'heading',
          attrs: {
            level: 2,
          },
          content: [
            {
              text: 'Origins of cannabis',
              type: 'text',
            },
          ],
        },
        {
          type: 'paragraph',
          content: [
            {
              text:
                'According to Scott Riefler, Chief Science Officer at SōRSE Technology, the answe' +
                'r is yes. “If the cannabinoid is introduced in an appropriate manner … we would ' +
                'not expect it to alter the shelf life of the food platform itself.” Meaning, for' +
                ' example, that a cannabis-infused chocolate bar should last as long as a traditi' +
                'onal one.',
              type: 'text',
            },
          ],
        },
        {
          type: 'blok',
          attrs: {
            id: '31bab7f8-f086-4693-8085-eaa054aa165f',
            body: [
              {
                _uid: 'i-7ffe4dc7-2989-43df-b55f-a2ccee482c2f',
                name: 'text feature',
                text: 'this is a text feature',
                layout: 'feature-full',
                component: 'feature',
              },
            ],
          },
        },
        {
          type: 'paragraph',
          content: [
            {
              text:
                'In the last article of our guide to CBD, you learned some general CBD terminolog' +
                'y that is often misunderstood, the three main types of CBD oil and the differenc' +
                'e between an oil and a tincture. Now that you know about the types of CBD concen' +
                'trates, you’ll learn the science behind CBD oil and its uses.',
              type: 'text',
            },
          ],
        },
        {
          type: 'blok',
          attrs: {
            id: '31bab7f8-f086-4693-8085-eaa054aa165f',
            body: [
              {
                name: 'image feature',
                image:
                  '//a.storyblok.com/f/81752/5456x3637/54d932ad99/cbd-leaf.jpg',
                layout: 'feature-left',
                component: 'feature',
              },
            ],
          },
        },
        {
          type: 'paragraph',
          content: [
            {
              text:
                'The Aussie CBD market is very different from other countries, in fact, it’s real' +
                'ly non-existent. However, it’s likely that if any type of cannabis were to becom' +
                'e legal in the near future, CBD oil would be the first product. The World Health' +
                ' Organisation (WHO) completed a review of studies on CBD in both humans and anim' +
                'als and found that CBD.',
              type: 'text',
            },
          ],
        },
        {
          type: 'paragraph',
          content: [
            {
              text:
                'In order to understand the benefits and side effects of CBD oil, it’s important ' +
                'you have a basic understanding of how CBD and the endocannabinoid system interac' +
                't. Because the endocannabinoid system plays a part in so many bodily processes, ' +
                'there seems to be an endless number of potential benefits of CBD oil. Here are s' +
                'ome of the main benefits people using CBD oil as medication and supplement all o' +
                'ver the world report experiencing.',
              type: 'text',
            },
          ],
        },
        {
          type: 'paragraph',
          content: [
            {
              text:
                'A poll that was conducted in Tasmania by the Greens shows overwhelming support f' +
                'or cannabis reform and decriminalisation of this beautiful plant. The National D' +
                'rug Strategy Household Survey in 2016 shows that most Australians support cannab' +
                'is reform in some way, and countless Australians would love to have cannabis ful' +
                'ly legalised. ',
              type: 'text',
            },
          ],
        },
        {
          type: 'heading',
          attrs: {
            level: 2,
          },
          content: [
            {
              text: 'What is CBD Oil?',
              type: 'text',
            },
          ],
        },
        {
          type: 'paragraph',
          content: [
            {
              text:
                'In order to understand the benefits and side effects of CBD oil, it’s important ' +
                'you have a basic understanding of how CBD and the endocannabinoid system interac' +
                't. Because the endocannabinoid system plays a part in so many bodily processes, ' +
                'there seems to be an endless number of potential benefits of CBD oil. Here are s' +
                'ome of the main benefits people using CBD oil as medication and supplement all o' +
                'ver the world report experiencing.',
              type: 'text',
            },
          ],
        },
        {
          type: 'paragraph',
          content: [
            {
              text:
                'A poll that was conducted in Tasmania by the Greens shows overwhelming support f' +
                'or cannabis reform and decriminalisation of this beautiful plant. The National D' +
                'rug Strategy Household Survey in 2016 shows that most Australians support cannab' +
                'is reform in some way, and countless Australians would love to have cannabis ful' +
                'ly legalised. ',
              type: 'text',
            },
          ],
        },
        {
          type: 'heading',
          attrs: {
            level: 2,
          },
          content: [
            {
              text: 'How can I access CBD?',
              type: 'text',
            },
          ],
        },
        {
          type: 'paragraph',
          content: [
            {
              text:
                'In order to understand the benefits and side effects of CBD oil, it’s important ' +
                'you have a basic understanding of how CBD and the endocannabinoid system interac' +
                't. Because the endocannabinoid system plays a part in so many bodily processes, ' +
                'there seems to be an endless number of potential benefits of CBD oil. Here are s' +
                'ome of the main benefits people using CBD oil as medication and supplement all o' +
                'ver the world report experiencing.',
              type: 'text',
            },
          ],
        },
        {
          type: 'heading',
          attrs: {
            level: 3,
          },
          content: [
            {
              text: 'Is it legal in Australia?',
              type: 'text',
            },
          ],
        },
        {
          type: 'paragraph',
          content: [
            {
              text:
                'A poll that was conducted in Tasmania by the Greens shows overwhelming support f' +
                'or cannabis reform and decriminalisation of this beautiful plant. The National D' +
                'rug Strategy Household Survey in 2016 shows that most Australians support cannab' +
                'is reform in some way, and countless Australians would love to have cannabis ful' +
                'ly legalised. ',
              type: 'text',
            },
          ],
        },
      ],
    },
  },
};

export const searchStories = [
  {
    name: 'A brief history of cannabis',
    created_at: '2020-04-23T01:10:19.611Z',
    published_at: '2020-04-23T01:28:32.865Z',
    alternates: [],
    id: 10348918,
    uuid: '81a0386c-49c8-4c44-bb0c-f6beb074e559',
    content: {
      _uid: 'feb50962-72af-4c8c-898c-13af6b33f713',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'We’re living in the Golden Age of Branding. There are more ways than ever for bu' +
                  'sinesses to carve out their niche in the marketplace and connect directly with t' +
                  'heir customers and fans. But the history of branding actually goes back centurie' +
                  's. This discipline and art form has evolved over the years to become an essentia' +
                  'l part of building any successful business.',
                type: 'text',
              },
            ],
          },
          {
            type: 'heading',
            attrs: {
              level: 2,
            },
            content: [
              {
                text: 'Origins of cannabis',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Branding actually begins in the 1500s, but major shifts took place in the 19th a' +
                  'nd 20th centuries. Through decades of experimentation and technological advancem' +
                  'ents, brands have learned how to break through the clutter and capture the atten' +
                  'tion of their customers, turning indifferent consumers into brand enthusiasts. L' +
                  'earning this fascinating backstory is a vital step in developing your own brand.',
                type: 'text',
              },
            ],
          },
        ],
      },
      title:
        'A brief history of cannabis and a really really really long heading plus more an' +
        'd more text in the heading too',
      subTitle: 'We dive into the origins of the mysterious herb',
      component: 'post',
      mainImage: '',
      thumbnail:
        '//a.storyblok.com/f/81752/3648x2736/9ec114dc8d/matteo-paganelli-mqiskm2ilgc-unsp' +
        'lash.jpg',
    },
    slug: 'a-brief-history-of-cannabis',
    full_slug: 'posts/a-brief-history-of-cannabis',
    sort_by_date: null,
    position: -30,
    tag_list: ['cannabis'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: '2a57db43-9b42-4577-b0ce-082ed48cbe17',
    first_published_at: '2020-04-23T01:14:16.128Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
  {
    name: 'What is CBD oil?',
    created_at: '2020-04-20T12:02:59.002Z',
    published_at: '2020-04-22T11:55:19.071Z',
    alternates: [],
    id: 10181154,
    uuid: '8c3b4e35-9764-43ef-be6f-06735cde67d3',
    content: {
      _uid: 'b810f9c9-21f3-40ab-a186-08e6feb5ac75',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'In the last article of our guide to CBD, you learned some general CBD terminolog' +
                  'y that is often misunderstood, the three main types of CBD oil and the differenc' +
                  'e between an oil and a tincture. Now that you know about the types of CBD concen' +
                  'trates, you’ll learn the science behind CBD oil and its uses.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'According to Scott Riefler, Chief Science Officer at SōRSE Technology, the answe' +
                  'r is yes. “If the cannabinoid is introduced in an appropriate manner … we would ' +
                  'not expect it to alter the shelf life of the food platform itself.” Meaning, for' +
                  ' example, that a cannabis-infused chocolate bar should last as long as a traditi' +
                  'onal one.',
                type: 'text',
              },
            ],
          },
          {
            type: 'heading',
            attrs: {
              level: 2,
            },
            content: [
              {
                text: 'Why is CBD oil so popular now?',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Starting from there, a lot will be common sense—check the best-by date on the pa' +
                  'ckage to ensure a nice long shelf life and maximum flavor and texture in the pro' +
                  'duct, and pay extra attention to the expiration date, which will indicate when a' +
                  ' product is no longer safe tovconsume. And much like foods found in every grocer' +
                  'y store, edibles with preservatives will last longer than those that are preserv' +
                  'ative-free.',
                type: 'text',
              },
            ],
          },
          {
            type: 'bullet_list',
            content: [
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'How the CBD craze began',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text:
                          'What the research about CBD oil says about:',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Epilepsy & seizures',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Chronic pain',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Nausea and vomiting',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Multiple sclerosis',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Sleep',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Overall, this is good news for those wanting to buy in bulk. “Most edibles have ' +
                  'very good shelf life,” said Riefler. “If you are stocking up, we would suggest t' +
                  'hat for any food platform, that you not buy beyond six months ahead.”',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'But what about for those who are crafty in the kitchen and want to make their ow' +
                  'n edibles?',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '9b3dfb1d-ff92-4e68-9087-0da41199ce47',
              body: [
                {
                  _uid: 'i-4cae5d26-8dce-4bf4-a898-4bda2902b00c',
                  name: '',
                  text:
                    'The cannabinoid content will not impact the shelf life of the food itself',
                  image: '',
                  layout: 'feature-right',
                  component: 'feature',
                },
              ],
            },
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  '“First, consider the non-infused counterpart of what you are trying to make; thi' +
                  'nk about how you store it and how long it takes you to consume it or use it. The' +
                  ' cannabinoid content will not impact the shelf life of [the] food itself,” said ' +
                  'Riefler.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'The research behind cannabis and nausea and vomiting is specific to these sympto' +
                  'ms as a result of chemotherapy. A 2017 report from the National Academies said t' +
                  'hat there was conclusive or substantial evidence that cannabis is effective in t' +
                  'he treatment of Nausea and Vomiting during Chemotherapy.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'A systematic review of 30 clinical trials showed that oral cannabinoids (in this' +
                  ' case synthetic) are more effective at treating chemo-induced nausea than tradit' +
                  'ional drugs. It also concluded that while patients preferred cannabis to other t' +
                  'reatments, there are some serious adverse effects. Some of the adverse effects, ' +
                  'such as feeling euphoria or sedation, could be seen to be beneficial to a patien' +
                  't in pain. Other effects, depression and paranoia, are reasons to avoid using ca' +
                  'nnabis as a treatment.',
                type: 'text',
              },
            ],
          },
        ],
      },
      title: 'What is CBD oil?',
      subTitle:
        'What is the mysterious CBD oil and what is all the fuss about? We have a deep di' +
        've into CBD.',
      component: 'post',
      mainImage:
        '//a.storyblok.com/f/81752/5456x3637/54d932ad99/cbd-leaf.jpg',
      thumbnail: '',
    },
    slug: 'what-is-cbd-oil',
    full_slug: 'posts/what-is-cbd-oil',
    sort_by_date: null,
    position: -20,
    tag_list: ['cbd', 'cannabis'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: '18d679c0-33f5-438e-8826-e77d4e10b97a',
    first_published_at: '2020-04-20T12:12:15.000Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
  {
    name: 'How long do cannabis edibles stay fresh?',
    created_at: '2020-04-20T05:46:22.980Z',
    published_at: '2020-04-22T11:15:47.023Z',
    alternates: [],
    id: 10153077,
    uuid: '36651e60-5566-40fe-8183-96cfec846d83',
    content: {
      _uid: '82270d9a-c44c-456c-950b-0e7ff38bda53',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Hello, this is the second postaAccording to Scott Riefler, Chief Science Officer' +
                  ' at SōRSE Technology, the answer is yes. “If the cannabinoid is introduced in an' +
                  ' appropriate manner … we would not expect it to alter the shelf life of the food' +
                  ' platform itself.” Meaning, for example, that a cannabis-infused chocolate bar s' +
                  'hould last as long as a traditional one. ',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Starting from there, a lot will be common sense—check the best-by date on the pa' +
                  'ckage to ensure a nice long shelf life and maximum flavor and texture in the pro' +
                  'duct, and pay extra attention to the expiration date, which will indicate when a' +
                  ' product is no longer safe to consume. And much like foods found in every grocer' +
                  'y store, edibles with preservatives will last longer than those that are preserv' +
                  'ative-free.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'So how rapidly will potency in an edible degrade? Riefler explained, “Potency of' +
                  ' THC tends to degrade very slowly with time. The shelf date [or] expiration date' +
                  ' speaks to the food platform, not the cannabinoid as an ingredient. If we are ta' +
                  'lking about a three to six month window of time, the potency should remain the s' +
                  'ame unless it’s being abused in storage … in edible form, THC will retain its ef' +
                  'ficacy up to six months.” ',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'When asked if all cannabinoids are created equal, Riefler said “CBD is a very st' +
                  'able cannabinoid. In contrast, THC does degrade slowly over time.”',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'But what about the differences between various types of edibles? ',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  '“What can contribute to degradation is oxygen. In a material that is solid in fo' +
                  'rm, like a gummy or lozenge, the availability of oxygen in the interior is much ' +
                  'less than that of a bottle of a beverage,” said Riefler. “What also might accele' +
                  'rate loss of potency might include very acidic or basic environments. The method' +
                  ' of presenting the cannabinoid into the edible matters.”',
                type: 'text',
              },
            ],
          },
        ],
      },
      title: 'How long do cannabis edibles stay fresh?',
      subTitle:
        'Edibles are already a staple of many people’s cannabis lineup',
      component: 'post',
      mainImage:
        '//a.storyblok.com/f/81752/4708x3531/c21708a784/pharma-hemp-complex-w63jsu4zkbw-u' +
        'nsplash.jpg',
    },
    slug: 'how-long-do-cannabis-edibles-stay-fresh',
    full_slug: 'posts/how-long-do-cannabis-edibles-stay-fresh',
    sort_by_date: null,
    position: -10,
    tag_list: ['gummies', 'cannabis', 'edibles'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: 'ae893e7f-864f-4d87-bdbd-55295d452e2d',
    first_published_at: '2020-04-20T05:46:53.000Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
  {
    name: 'Understanding cannabis law in Australia',
    created_at: '2020-04-20T02:26:24.168Z',
    published_at: '2020-04-22T11:35:42.182Z',
    alternates: [],
    id: 10152274,
    uuid: '8ea41486-dcfc-45ea-add1-652422fbe9ac',
    content: {
      _uid: 'faf33522-22c9-4e97-a9f9-10b6c95fd3aa',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'When cannabis smoke or vapor is inhaled, cannabinoids like THC are absorbed into' +
                  ' the lungs and enter the bloodstream. But how much THC do we actually absorb? We' +
                  'll, that depends.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Researchers from The British Journal of Anesthesia in 1999 reported, “Approximat' +
                  'ely 50% of the THC and other cannabinoids present in a cannabis cigarette enter ' +
                  'the mainstream smoke and are inhaled. The amount absorbed through the lungs depe' +
                  'nds on smoking style. In experienced smokers, who inhaled deeply and hold the sm' +
                  'oke in the lungs for some seconds before exhaling, virtually all of the cannabin' +
                  'oids present in the mainstream smoke enter the bloodstream.”',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '31bab7f8-f086-4693-8085-eaa054aa165f',
              body: [
                {
                  _uid: 'i-7ffe4dc7-2989-43df-b55f-a2ccee482c2f',
                  body: {
                    type: 'doc',
                    content: [
                      {
                        type: 'paragraph',
                        content: [
                          {
                            text: 'some text in here!',
                            type: 'text',
                          },
                        ],
                      },
                    ],
                  },
                  name: 'text feature',
                  text: 'this is a text feature',
                  image: '',
                  layout: 'feature-full',
                  component: 'feature',
                },
              ],
            },
          },
          {
            type: 'paragraph',
            content: [
              {
                text: 'bla blah',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '017911b2-2800-4e63-abdc-482f219e791b',
              body: [
                {
                  _uid: 'i-135205f7-c6ce-4361-8a6d-5828433c6e50',
                  body: {
                    type: 'doc',
                    content: [
                      {
                        type: 'paragraph',
                        content: [
                          {
                            type: 'image',
                            attrs: {
                              alt: '',
                              src:
                                '//a.storyblok.com/f/81752/5456x3637/a21bdd6175/cbd-leaf.jpg',
                              title: null,
                            },
                          },
                        ],
                      },
                    ],
                  },
                  name: 'cbd',
                  text: '',
                  image:
                    '//a.storyblok.com/f/81752/5456x3637/a21bdd6175/cbd-leaf.jpg',
                  layout: 'feature-left',
                  component: 'feature',
                },
              ],
            },
          },
        ],
      },
      title: 'Understanding cannabis law in Australia',
      subTitle:
        'What are the laws surrounding cannabis in each state?',
      component: 'post',
      mainImage:
        '//a.storyblok.com/f/81752/6720x4480/4458f2f751/thought-catalog-uoxlndt32hg-unspl' +
        'ash.jpg',
      thumbnail:
        '//a.storyblok.com/f/81752/6720x4480/4458f2f751/thought-catalog-uoxlndt32hg-unspl' +
        'ash.jpg',
    },
    slug: 'understanding-cannabis-law-in-australia',
    full_slug: 'posts/understanding-cannabis-law-in-australia',
    sort_by_date: null,
    position: 0,
    tag_list: ['test2', 'test'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: '56e8eb98-2dc9-4fa4-9058-ff567e89bbd9',
    first_published_at: '2020-04-20T02:27:14.000Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
  {
    name: 'Another Story',
    created_at: '2020-04-20T02:26:24.168Z',
    published_at: '2020-04-22T11:35:42.182Z',
    alternates: [],
    id: 10152274,
    uuid: '8ea41486-dcfc-45ea-add1-652422fbe9ac',
    content: {
      _uid: 'faf33522-22c9-4e97-a9f9-10b6c95fd3aa',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'When cannabis smoke or vapor is inhaled, cannabinoids like THC are absorbed into' +
                  ' the lungs and enter the bloodstream. But how much THC do we actually absorb? We' +
                  'll, that depends.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Researchers from The British Journal of Anesthesia in 1999 reported, “Approximat' +
                  'ely 50% of the THC and other cannabinoids present in a cannabis cigarette enter ' +
                  'the mainstream smoke and are inhaled. The amount absorbed through the lungs depe' +
                  'nds on smoking style. In experienced smokers, who inhaled deeply and hold the sm' +
                  'oke in the lungs for some seconds before exhaling, virtually all of the cannabin' +
                  'oids present in the mainstream smoke enter the bloodstream.”',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '31bab7f8-f086-4693-8085-eaa054aa165f',
              body: [
                {
                  _uid: 'i-7ffe4dc7-2989-43df-b55f-a2ccee482c2f',
                  body: {
                    type: 'doc',
                    content: [
                      {
                        type: 'paragraph',
                        content: [
                          {
                            text: 'some text in here!',
                            type: 'text',
                          },
                        ],
                      },
                    ],
                  },
                  name: 'text feature',
                  text: 'this is a text feature',
                  image: '',
                  layout: 'feature-full',
                  component: 'feature',
                },
              ],
            },
          },
          {
            type: 'paragraph',
            content: [
              {
                text: 'bla blah',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '017911b2-2800-4e63-abdc-482f219e791b',
              body: [
                {
                  _uid: 'i-135205f7-c6ce-4361-8a6d-5828433c6e50',
                  body: {
                    type: 'doc',
                    content: [
                      {
                        type: 'paragraph',
                        content: [
                          {
                            type: 'image',
                            attrs: {
                              alt: '',
                              src:
                                '//a.storyblok.com/f/81752/5456x3637/a21bdd6175/cbd-leaf.jpg',
                              title: null,
                            },
                          },
                        ],
                      },
                    ],
                  },
                  name: 'cbd',
                  text: '',
                  image:
                    '//a.storyblok.com/f/81752/5456x3637/a21bdd6175/cbd-leaf.jpg',
                  layout: 'feature-left',
                  component: 'feature',
                },
              ],
            },
          },
        ],
      },
      title: 'Another cannabis story',
      subTitle:
        'What are the laws surrounding cannabis in each state?',
      component: 'post',
      thumbnail:
        '//a.storyblok.com/f/81752/5000x3334/61ff955732/add-weed-bu6bsersl_m-unsplash.jpg',
    },
    slug: 'understanding-cannabis-law-in-australia',
    full_slug: 'posts/understanding-cannabis-law-in-australia',
    sort_by_date: null,
    position: 0,
    tag_list: ['test2', 'test'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: '56e8eb98-2dc9-4fa4-9058-ff567e89bbd9',
    first_published_at: '2020-04-20T02:27:14.000Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
];

export const fetchStories = {
  '0': {
    name: 'A brief history of cannabis',
    created_at: '2020-04-23T01:10:19.611Z',
    published_at: '2020-04-23T01:28:32.865Z',
    alternates: [],
    id: 10348918,
    uuid: '81a0386c-49c8-4c44-bb0c-f6beb074e559',
    content: {
      _uid: 'feb50962-72af-4c8c-898c-13af6b33f713',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'We’re living in the Golden Age of Branding. There are more ways than ever for bu' +
                  'sinesses to carve out their niche in the marketplace and connect directly with t' +
                  'heir customers and fans. But the history of branding actually goes back centurie' +
                  's. This discipline and art form has evolved over the years to become an essentia' +
                  'l part of building any successful business.',
                type: 'text',
              },
            ],
          },
          {
            type: 'heading',
            attrs: {
              level: 2,
            },
            content: [
              {
                text: 'Origins of cannabis',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Branding actually begins in the 1500s, but major shifts took place in the 19th a' +
                  'nd 20th centuries. Through decades of experimentation and technological advancem' +
                  'ents, brands have learned how to break through the clutter and capture the atten' +
                  'tion of their customers, turning indifferent consumers into brand enthusiasts. L' +
                  'earning this fascinating backstory is a vital step in developing your own brand.',
                type: 'text',
              },
            ],
          },
        ],
      },
      title:
        'A brief history of cannabis and a really really really long heading plus more an' +
        'd more text in the heading too',
      subTitle: 'We dive into the origins of the mysterious herb',
      component: 'post',
      mainImage: '',
      thumbnail:
        '//a.storyblok.com/f/81752/3648x2736/9ec114dc8d/matteo-paganelli-mqiskm2ilgc-unsp' +
        'lash.jpg',
    },
    slug: 'a-brief-history-of-cannabis',
    full_slug: 'posts/a-brief-history-of-cannabis',
    sort_by_date: null,
    position: -30,
    tag_list: ['cannabis'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: '2a57db43-9b42-4577-b0ce-082ed48cbe17',
    first_published_at: '2020-04-23T01:14:16.128Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
  '1': {
    name: 'What is CBD oil?',
    created_at: '2020-04-20T12:02:59.002Z',
    published_at: '2020-04-22T11:55:19.071Z',
    alternates: [],
    id: 10181154,
    uuid: '8c3b4e35-9764-43ef-be6f-06735cde67d3',
    content: {
      _uid: 'b810f9c9-21f3-40ab-a186-08e6feb5ac75',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'In the last article of our guide to CBD, you learned some general CBD terminolog' +
                  'y that is often misunderstood, the three main types of CBD oil and the differenc' +
                  'e between an oil and a tincture. Now that you know about the types of CBD concen' +
                  'trates, you’ll learn the science behind CBD oil and its uses.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'According to Scott Riefler, Chief Science Officer at SōRSE Technology, the answe' +
                  'r is yes. “If the cannabinoid is introduced in an appropriate manner … we would ' +
                  'not expect it to alter the shelf life of the food platform itself.” Meaning, for' +
                  ' example, that a cannabis-infused chocolate bar should last as long as a traditi' +
                  'onal one.',
                type: 'text',
              },
            ],
          },
          {
            type: 'heading',
            attrs: {
              level: 2,
            },
            content: [
              {
                text: 'Why is CBD oil so popular now?',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Starting from there, a lot will be common sense—check the best-by date on the pa' +
                  'ckage to ensure a nice long shelf life and maximum flavor and texture in the pro' +
                  'duct, and pay extra attention to the expiration date, which will indicate when a' +
                  ' product is no longer safe tovconsume. And much like foods found in every grocer' +
                  'y store, edibles with preservatives will last longer than those that are preserv' +
                  'ative-free.',
                type: 'text',
              },
            ],
          },
          {
            type: 'bullet_list',
            content: [
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'How the CBD craze began',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text:
                          'What the research about CBD oil says about:',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Epilepsy & seizures',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Chronic pain',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Nausea and vomiting',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Multiple sclerosis',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'list_item',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        text: 'Sleep',
                        type: 'text',
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Overall, this is good news for those wanting to buy in bulk. “Most edibles have ' +
                  'very good shelf life,” said Riefler. “If you are stocking up, we would suggest t' +
                  'hat for any food platform, that you not buy beyond six months ahead.”',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'But what about for those who are crafty in the kitchen and want to make their ow' +
                  'n edibles?',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '9b3dfb1d-ff92-4e68-9087-0da41199ce47',
              body: [
                {
                  _uid: 'i-4cae5d26-8dce-4bf4-a898-4bda2902b00c',
                  name: '',
                  text:
                    'The cannabinoid content will not impact the shelf life of the food itself',
                  image: '',
                  layout: 'feature-right',
                  component: 'feature',
                },
              ],
            },
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  '“First, consider the non-infused counterpart of what you are trying to make; thi' +
                  'nk about how you store it and how long it takes you to consume it or use it. The' +
                  ' cannabinoid content will not impact the shelf life of [the] food itself,” said ' +
                  'Riefler.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'The research behind cannabis and nausea and vomiting is specific to these sympto' +
                  'ms as a result of chemotherapy. A 2017 report from the National Academies said t' +
                  'hat there was conclusive or substantial evidence that cannabis is effective in t' +
                  'he treatment of Nausea and Vomiting during Chemotherapy.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'A systematic review of 30 clinical trials showed that oral cannabinoids (in this' +
                  ' case synthetic) are more effective at treating chemo-induced nausea than tradit' +
                  'ional drugs. It also concluded that while patients preferred cannabis to other t' +
                  'reatments, there are some serious adverse effects. Some of the adverse effects, ' +
                  'such as feeling euphoria or sedation, could be seen to be beneficial to a patien' +
                  't in pain. Other effects, depression and paranoia, are reasons to avoid using ca' +
                  'nnabis as a treatment.',
                type: 'text',
              },
            ],
          },
        ],
      },
      title: 'What is CBD oil?',
      subTitle:
        'What is the mysterious CBD oil and what is all the fuss about? We have a deep di' +
        've into CBD.',
      component: 'post',
      mainImage:
        '//a.storyblok.com/f/81752/5456x3637/54d932ad99/cbd-leaf.jpg',
      thumbnail: '',
    },
    slug: 'what-is-cbd-oil',
    full_slug: 'posts/what-is-cbd-oil',
    sort_by_date: null,
    position: -20,
    tag_list: ['cbd', 'cannabis'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: '18d679c0-33f5-438e-8826-e77d4e10b97a',
    first_published_at: '2020-04-20T12:12:15.000Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
  '2': {
    name: 'How long do cannabis edibles stay fresh?',
    created_at: '2020-04-20T05:46:22.980Z',
    published_at: '2020-04-22T11:15:47.023Z',
    alternates: [],
    id: 10153077,
    uuid: '36651e60-5566-40fe-8183-96cfec846d83',
    content: {
      _uid: '82270d9a-c44c-456c-950b-0e7ff38bda53',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Hello, this is the second postaAccording to Scott Riefler, Chief Science Officer' +
                  ' at SōRSE Technology, the answer is yes. “If the cannabinoid is introduced in an' +
                  ' appropriate manner … we would not expect it to alter the shelf life of the food' +
                  ' platform itself.” Meaning, for example, that a cannabis-infused chocolate bar s' +
                  'hould last as long as a traditional one. ',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Starting from there, a lot will be common sense—check the best-by date on the pa' +
                  'ckage to ensure a nice long shelf life and maximum flavor and texture in the pro' +
                  'duct, and pay extra attention to the expiration date, which will indicate when a' +
                  ' product is no longer safe to consume. And much like foods found in every grocer' +
                  'y store, edibles with preservatives will last longer than those that are preserv' +
                  'ative-free.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'So how rapidly will potency in an edible degrade? Riefler explained, “Potency of' +
                  ' THC tends to degrade very slowly with time. The shelf date [or] expiration date' +
                  ' speaks to the food platform, not the cannabinoid as an ingredient. If we are ta' +
                  'lking about a three to six month window of time, the potency should remain the s' +
                  'ame unless it’s being abused in storage … in edible form, THC will retain its ef' +
                  'ficacy up to six months.” ',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'When asked if all cannabinoids are created equal, Riefler said “CBD is a very st' +
                  'able cannabinoid. In contrast, THC does degrade slowly over time.”',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'But what about the differences between various types of edibles? ',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  '“What can contribute to degradation is oxygen. In a material that is solid in fo' +
                  'rm, like a gummy or lozenge, the availability of oxygen in the interior is much ' +
                  'less than that of a bottle of a beverage,” said Riefler. “What also might accele' +
                  'rate loss of potency might include very acidic or basic environments. The method' +
                  ' of presenting the cannabinoid into the edible matters.”',
                type: 'text',
              },
            ],
          },
        ],
      },
      title: 'How long do cannabis edibles stay fresh?',
      subTitle:
        'Edibles are already a staple of many people’s cannabis lineup',
      component: 'post',
      mainImage:
        '//a.storyblok.com/f/81752/4708x3531/c21708a784/pharma-hemp-complex-w63jsu4zkbw-u' +
        'nsplash.jpg',
    },
    slug: 'how-long-do-cannabis-edibles-stay-fresh',
    full_slug: 'posts/how-long-do-cannabis-edibles-stay-fresh',
    sort_by_date: null,
    position: -10,
    tag_list: ['gummies', 'cannabis', 'edibles'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: 'ae893e7f-864f-4d87-bdbd-55295d452e2d',
    first_published_at: '2020-04-20T05:46:53.000Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
  '3': {
    name: 'Understanding cannabis law in Australia',
    created_at: '2020-04-20T02:26:24.168Z',
    published_at: '2020-04-22T11:35:42.182Z',
    alternates: [],
    id: 10152274,
    uuid: '8ea41486-dcfc-45ea-add1-652422fbe9ac',
    content: {
      _uid: 'faf33522-22c9-4e97-a9f9-10b6c95fd3aa',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'When cannabis smoke or vapor is inhaled, cannabinoids like THC are absorbed into' +
                  ' the lungs and enter the bloodstream. But how much THC do we actually absorb? We' +
                  'll, that depends.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Researchers from The British Journal of Anesthesia in 1999 reported, “Approximat' +
                  'ely 50% of the THC and other cannabinoids present in a cannabis cigarette enter ' +
                  'the mainstream smoke and are inhaled. The amount absorbed through the lungs depe' +
                  'nds on smoking style. In experienced smokers, who inhaled deeply and hold the sm' +
                  'oke in the lungs for some seconds before exhaling, virtually all of the cannabin' +
                  'oids present in the mainstream smoke enter the bloodstream.”',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '31bab7f8-f086-4693-8085-eaa054aa165f',
              body: [
                {
                  _uid: 'i-7ffe4dc7-2989-43df-b55f-a2ccee482c2f',
                  body: {
                    type: 'doc',
                    content: [
                      {
                        type: 'paragraph',
                        content: [
                          {
                            text: 'some text in here!',
                            type: 'text',
                          },
                        ],
                      },
                    ],
                  },
                  name: 'text feature',
                  text: 'this is a text feature',
                  image: '',
                  layout: 'feature-full',
                  component: 'feature',
                },
              ],
            },
          },
          {
            type: 'paragraph',
            content: [
              {
                text: 'bla blah',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '017911b2-2800-4e63-abdc-482f219e791b',
              body: [
                {
                  _uid: 'i-135205f7-c6ce-4361-8a6d-5828433c6e50',
                  body: {
                    type: 'doc',
                    content: [
                      {
                        type: 'paragraph',
                        content: [
                          {
                            type: 'image',
                            attrs: {
                              alt: '',
                              src:
                                '//a.storyblok.com/f/81752/5456x3637/a21bdd6175/cbd-leaf.jpg',
                              title: null,
                            },
                          },
                        ],
                      },
                    ],
                  },
                  name: 'cbd',
                  text: '',
                  image:
                    '//a.storyblok.com/f/81752/5456x3637/a21bdd6175/cbd-leaf.jpg',
                  layout: 'feature-left',
                  component: 'feature',
                },
              ],
            },
          },
        ],
      },
      title: 'Understanding cannabis law in Australia',
      subTitle:
        'What are the laws surrounding cannabis in each state?',
      component: 'post',
      mainImage:
        '//a.storyblok.com/f/81752/6720x4480/4458f2f751/thought-catalog-uoxlndt32hg-unspl' +
        'ash.jpg',
      thumbnail:
        '//a.storyblok.com/f/81752/6720x4480/4458f2f751/thought-catalog-uoxlndt32hg-unspl' +
        'ash.jpg',
    },
    slug: 'understanding-cannabis-law-in-australia',
    full_slug: 'posts/understanding-cannabis-law-in-australia',
    sort_by_date: null,
    position: 0,
    tag_list: ['test2', 'test'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: '56e8eb98-2dc9-4fa4-9058-ff567e89bbd9',
    first_published_at: '2020-04-20T02:27:14.000Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
  '4': {
    name: 'Another Story',
    created_at: '2020-04-20T02:26:24.168Z',
    published_at: '2020-04-22T11:35:42.182Z',
    alternates: [],
    id: 10152274,
    uuid: '8ea41486-dcfc-45ea-add1-652422fbe9ac',
    content: {
      _uid: 'faf33522-22c9-4e97-a9f9-10b6c95fd3aa',
      body: {
        type: 'doc',
        content: [
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'When cannabis smoke or vapor is inhaled, cannabinoids like THC are absorbed into' +
                  ' the lungs and enter the bloodstream. But how much THC do we actually absorb? We' +
                  'll, that depends.',
                type: 'text',
              },
            ],
          },
          {
            type: 'paragraph',
            content: [
              {
                text:
                  'Researchers from The British Journal of Anesthesia in 1999 reported, “Approximat' +
                  'ely 50% of the THC and other cannabinoids present in a cannabis cigarette enter ' +
                  'the mainstream smoke and are inhaled. The amount absorbed through the lungs depe' +
                  'nds on smoking style. In experienced smokers, who inhaled deeply and hold the sm' +
                  'oke in the lungs for some seconds before exhaling, virtually all of the cannabin' +
                  'oids present in the mainstream smoke enter the bloodstream.”',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '31bab7f8-f086-4693-8085-eaa054aa165f',
              body: [
                {
                  _uid: 'i-7ffe4dc7-2989-43df-b55f-a2ccee482c2f',
                  body: {
                    type: 'doc',
                    content: [
                      {
                        type: 'paragraph',
                        content: [
                          {
                            text: 'some text in here!',
                            type: 'text',
                          },
                        ],
                      },
                    ],
                  },
                  name: 'text feature',
                  text: 'this is a text feature',
                  image: '',
                  layout: 'feature-full',
                  component: 'feature',
                },
              ],
            },
          },
          {
            type: 'paragraph',
            content: [
              {
                text: 'bla blah',
                type: 'text',
              },
            ],
          },
          {
            type: 'blok',
            attrs: {
              id: '017911b2-2800-4e63-abdc-482f219e791b',
              body: [
                {
                  _uid: 'i-135205f7-c6ce-4361-8a6d-5828433c6e50',
                  body: {
                    type: 'doc',
                    content: [
                      {
                        type: 'paragraph',
                        content: [
                          {
                            type: 'image',
                            attrs: {
                              alt: '',
                              src:
                                '//a.storyblok.com/f/81752/5456x3637/a21bdd6175/cbd-leaf.jpg',
                              title: null,
                            },
                          },
                        ],
                      },
                    ],
                  },
                  name: 'cbd',
                  text: '',
                  image:
                    '//a.storyblok.com/f/81752/5456x3637/a21bdd6175/cbd-leaf.jpg',
                  layout: 'feature-left',
                  component: 'feature',
                },
              ],
            },
          },
        ],
      },
      title: 'Another cannabis story',
      subTitle:
        'What are the laws surrounding cannabis in each state?',
      component: 'post',
      thumbnail:
        '//a.storyblok.com/f/81752/5000x3334/61ff955732/add-weed-bu6bsersl_m-unsplash.jpg',
    },
    slug: 'understanding-cannabis-law-in-australia',
    full_slug: 'posts/understanding-cannabis-law-in-australia',
    sort_by_date: null,
    position: 0,
    tag_list: ['test2', 'test'],
    is_startpage: false,
    parent_id: 10147472,
    meta_data: null,
    group_id: '56e8eb98-2dc9-4fa4-9058-ff567e89bbd9',
    first_published_at: '2020-04-20T02:27:14.000Z',
    release_id: null,
    lang: 'default',
    path: null,
    translated_slugs: [],
  },
};

export const fetchMockStrain = {
  id: 'jPNxPgN9',
  name: 'Blue Dream',
  sativaPct: 60,
  indicaPct: 40,
  category: 'Sativa Dominant',
  aka: null,
  stimulateScore: 62,
  description:
    'Blue Dream, a sativa-dominant hybrid originating in California, has achieved legendary status among West Coast strains. Crossing Blueberry with Haze, Blue Dream balances full-body relaxation with gentle cerebral invigoration. Novice and veteran consumers alike enjoy the level effects of Blue Dream, which ease you gently into a calm euphoria. \n\nWith a sweet berry aroma redolent of its Blueberry parent, Blue Dream delivers swift symptom relief without heavy sedative effects. This makes Blue Dream a popular daytime medicine for patients treating pain, depression, nausea, and other ailments requiring a high THC strain. ',
  popularity: 0,
  cannabinoids: {
    thc: {
      min: 16.5,
      avg: 19,
      max: 22,
    },
    cbd: {
      min: 2,
      avg: 2,
      max: 2,
    },
    cbn: {
      min: 1,
      avg: 1,
      max: 1,
    },
    cbc: {
      min: null,
      avg: 0.050000000745058,
      max: null,
    },
    cbg: {
      min: null,
      avg: 0.279808644644916,
      max: null,
    },
    thcv: {
      min: null,
      avg: 0.019999999552965,
      max: null,
    },
  },
  terps: [
    {
      name: 'Myrcene',
      score: 0.386756049094848,
    },
    {
      name: 'Pinene',
      score: 0.32610858506828,
    },
    {
      name: 'Caryophyllene',
      score: 0.132025453906017,
    },
    {
      name: 'Limonene',
      score: 0.051309139347125,
    },
    {
      name: 'Humulene',
      score: 0.049655433661289,
    },
    {
      name: 'Linalool',
      score: 0.028688107349007,
    },
    {
      name: 'Terpinolene',
      score: 0.014183589652913,
    },
    {
      name: 'Ocimene',
      score: 0.011273641920521,
    },
  ],
  effects: {
    positives: [
      'Relaxing',
      'Uplifting',
      'Creative',
      'Energizing',
      'Euphoria',
      'Happy',
    ],
    helps: [
      'Migraines',
      'ADD/ADHD',
      'Anxiety',
      'Autism',
      'Bipolar Disorder',
      'Chronic Pain',
      'Depression',
      'Headaches',
      'Inflammation',
      'Nausea',
      'PTSD',
      'Stress',
    ],
    negatives: [
      'Anxious',
      'Dizzy',
      'Dry eyes',
      'Dry mouth',
      'Headache',
      'Paranoid',
    ],
  },
  flavour: [
    'Earthy',
    'Flowery',
    'Pungent',
    'Pine',
    'Citrus',
    'Woody',
    'Berry',
    'Blueberry',
    'Fruity',
    'Herbal',
    'Sweet',
    'Vanilla',
  ],
  aromas: ['Earthy', 'Fruity', 'Sweet', 'Vanilla'],
  growInfo: {
    averageYield: 'High',
    difficulty: 'Moderate',
    environment: null,
    floweringDays: 67,
    growNotes:
      '<ul>\n\t<li>Well suited for sea of green (SOG)</li>\n\t<li>Yields typically reach up to 430g/m&sup2;</li>\n\t<li>Can exhibit different indica and sativa&nbsp;<a href="http://www.leafly.com/knowledge-center/cannabis-101/cannabis-genotypes-and-phenotypes-what-makes-a-strain-unique">phenotypes</a></li>\n\t<li>Known to be susceptible to powdery mildew and spider mites</li>\n\t<li>Can stretch when flowering</li>\n</ul>\n',
    growNotesPlain:
      'Well suited for sea of green (SOG)\n\tYields typically reach up to 430g/m²\n\tCan exhibit different indica and sativa phenotypes\n\tKnown to be susceptible to powdery mildew and spider mites\n\tCan stretch when flowering',
    height: 'Tall',
    outdoorFinish: 'Early - Mid October',
    preferredMedium: null,
  },
  lineage: {
    children: [
      {
        name: 'Pine Queen Dream',
        id: 'xGAwXpZN',
        slug: 'pine-queen-dream',
        category: 'Sativa Dominant',
      },
      {
        name: 'DJ Andy Williams',
        id: 'wGE7KA5N',
        slug: 'dj-andy-williams',
        category: 'Sativa Dominant',
      },
    ],
    parents: [
      {
        name: 'Haze',
        id: 'K4209GE1',
        slug: 'haze',
        category: 'Sativa Dominant',
      },
      {
        name: 'Blueberry',
        id: '18NBw725',
        slug: 'blueberry',
        category: 'Indica Dominant',
      },
    ],
  },
  similar: [
    {
      name: 'Maui Wowie',
    },
    {
      name: 'Strawberry Cough',
      id: 'z4Lw8NWJ',
      slug: 'strawberry-cough',
      category: 'Sativa Dominant',
    },
    {
      name: 'Super Blue Dream',
      id: 'jENb5WLe',
      slug: 'super-blue-dream',
      category: 'Hybrid',
    },
    {
      name: 'Alaskan Thunder Fuck',
      id: 'bxG6qY2W',
      slug: 'alaskan-thunder-fuck',
      category: 'Sativa Dominant',
    },
    {
      name: 'Blue Haze',
      id: '4LwAvW2W',
      slug: 'blue-haze',
      category: 'Indica Dominant',
    },
    {
      name: 'White Widow',
      id: 'WdN1XLQn',
      slug: 'white-widow',
      category: 'Sativa Dominant',
    },
    {
      name: 'Tangie',
      id: '9GPoKk2d',
      slug: 'tangie',
      category: 'Sativa',
    },
    {
      name: 'Double Dream',
      id: 'J2qkkWLz',
      slug: 'double-dream',
      category: 'Sativa Dominant',
    },
  ],
  slug: 'blue-dream',
};
