import fetch from 'isomorphic-unfetch';
import queryString from 'query-string';
import StoryBlok from '~/utils/storyBlok';
// import strainList from './mocks/strainList';
import { serverHost } from '~/config';
import {
  fetchStory,
  fetchStories,
  searchStories,
  fetchMockStrain,
} from './mocks/storyblok';

export async function fetchPost(slug, folder = 'posts') {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await StoryBlok.get(
        `cdn/stories/${folder}/${slug}`
      );
      resolve(res ? res.data.story : {});
      // resolve(fetchStory);
    } catch (e) {
      console.error('Error', e);
    }
  });
}

export async function fetchPosts(params = {}) {
  try {
    const res = await StoryBlok.getStories(params);
    return res && res.length ? res : [];
    // return fetchStories;
  } catch (e) {
    console.error('Error', e);
  }
}

export async function fetchPostsWithTags(tags, excluding_slugs) {
  const tagString = tags.join(',');
  return new Promise(async (resolve, reject) => {
    try {
      const res = await StoryBlok.get('cdn/stories', {
        excluding_slugs,
        with_tag: tagString,
      });
      resolve(
        res && res.data && res.data.stories && res.data.stories.length
          ? res.data.stories
          : []
      );
    } catch (e) {
      console.error('Error', e);
    }
  });
}

export async function searchPosts(q) {
  try {
    const res = await StoryBlok.getStories({ search_term: q });
    return res && res.length ? res : [];
    // return searchStories;
  } catch (e) {
    console.error('Error', e);
  }
}

export async function fetchStrain(slug) {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await fetch(`${serverHost}/strains/${slug}`);
      const data = await res.json();
      resolve(data);
      // resolve(fetchMockStrain);
    } catch (e) {
      console.error('Error', e);
    }
  });
}

export async function fetchStrains(filter = {}) {
  const {
    _page = 1,
    _sort = 'popularity',
    _order,
    _limit = 24,
  } = filter;

  const query = queryString.stringify({
    _page,
    _sort,
    _order,
    _limit,
  });
  return new Promise(async (resolve, reject) => {
    try {
      const res = await fetch(`${serverHost}/strains?${query}`);
      const data = await res.json();
      const total = await res.headers.get('X-Total-Count');
      resolve({ data, total });
    } catch (e) {
      console.error('Error', e);
    }
  });
}

function uniqueStrains(strain, index, self) {
  return self.findIndex((d) => d.id === strain.id) === index;
}

export async function searchStrains(query, _page = 1) {
  return new Promise(async (resolve, reject) => {
    try {
      let searchQuery = query;
      const queryWords = query.split(' ');
      let queryRegex = '';
      if (queryWords.length) {
        queryWords.forEach((word) => {
          queryRegex += `(?=.*${word})`;
        });
      }

      const queryStr = queryString.stringify({
        _page,
        _limit: 24,
        q: query,
      });

      const res = await fetch(
        `${serverHost}/searchStrains?${queryStr}`
      );
      const data = (await res.json()) || {};
      const { results, totalCount = [] } = data;
      const total = totalCount[0] ? totalCount[0].count : 0;

      const totalMatch = [];
      const partialMatch = [];

      const sortedData = results.filter((d, i) => {
        const nameRegex = RegExp(queryRegex, 'gi');
        if (query.toLowerCase() === d.name.toLowerCase()) {
          totalMatch.push(results[i]);
          return false;
        }
        if (nameRegex.test(d.name)) {
          partialMatch.push(results[i]);
          return false;
        }
        return true;
      });
      const result = totalMatch.concat(partialMatch, sortedData);

      resolve({ data: result, total });
    } catch (e) {
      console.error('Error', e);
    }
  });
}

export async function submitContact(user, captcha) {
  return new Promise(async (resolve, reject) => {
    const res = await fetch(`${serverHost}contact`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: user.name,
        email: user.email,
        message: user.message,
        captcha,
      }),
    });
    if (res.status === 200) {
      return resolve();
    } else {
      return reject();
    }
  });
}

export async function fetchDoctors(filter = {}) {
  const { _sort = 'name', _order, state, category } = filter;

  let query = queryString.stringify({ _sort, _order });
  if (state) {
    query += `&service.${state}_like=true|tele`;
  }

  if (category) {
    category.split(',').forEach((c) => {
      query += `&category=${c}`;
    });
  }

  return new Promise(async (resolve, reject) => {
    try {
      const res = await fetch(`${serverHost}/doctors?${query}`);
      const data = await res.json();
      const total = await res.headers.get('X-Total-Count');
      resolve({ data, total });
    } catch (e) {
      console.error('Error', e);
    }
  });
}
