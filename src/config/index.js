export const layoutSizes = {
  span: 18,
  xs: 23,
  md: 22,
  lg: 20,
  xl: 18,
};

export const serverHost =
  process.env.NODE_ENV === 'development'
    ? 'http://localhost:3004/api'
    : 'https://api.budherd.com.au/api';
