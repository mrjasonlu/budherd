import StoryblokClient from 'storyblok-js-client';
import componentResolver from './componentResolver';

class StoryblokService {
  constructor() {
    this.devMode = false; // Always loads draft
    this.token =
      process.env.NODE_ENV === 'development'
        ? process.env.API_TOKEN_PREVIEW
        : process.env.API_TOKEN;
    this.client = new StoryblokClient({
      accessToken: this.token,
      cache: {
        clear: 'auto',
        type: 'memory',
      },
    });
    this.client.setComponentResolver(componentResolver);
    this.query = {};
  }

  getCacheVersion() {
    return this.client.cacheVersion;
  }

  render(data) {
    return (
      <div
        dangerouslySetInnerHTML={{
          __html: this.client.richTextResolver.render(data),
        }}
      />
    );
  }

  richTextResolver(data) {
    return this.client.richTextResolver.render(data);
  }

  get(slug, params) {
    params = params || {};

    if (process.env.NODE_ENV === 'development') {
      params.version = 'draft';
    }

    if (
      typeof window !== 'undefined' &&
      typeof window.StoryblokCacheVersion !== 'undefined'
    ) {
      params.cv = window.StoryblokCacheVersion;
    }

    return this.client.get(slug, params);
  }

  getStories(params = {}) {
    if (
      typeof window !== 'undefined' &&
      typeof window.StoryblokCacheVersion !== 'undefined'
    ) {
      params.cv = window.StoryblokCacheVersion;
    }

    if (process.env.NODE_ENV === 'development') {
      params.version = 'draft';
    }

    return this.client.getAll('cdn/stories', params);
  }

  setQuery(query) {
    this.query = query;
  }

  getQuery(param) {
    return this.query[param];
  }

  getSrcSet(url, webp) {
    return `${resize(url, '300x0', webp)} 300w, ${resize(
      url,
      '500x0',
      webp
    )} 500w, ${resize(url, '900x0', webp)} 900w, ${resize(
      url,
      '1200x0',
      webp
    )} 1200w`;
  }
}

function resize(image, option, webp = true) {
  var imageService = '//img2.storyblok.com/';
  image = image && image.replace && image.replace('https:', '');
  const setWebp = webp ? '/filters:format(webp)' : '';
  var path =
    image && image.replace && image.replace('//a.storyblok.com', '');
  return imageService + option + setWebp + path;
}

const storyblokInstance = new StoryblokService();

export default storyblokInstance;
