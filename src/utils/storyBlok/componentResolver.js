import renderFeature from './components/renderFeature';
import renderActionButton from './components/renderActionButton';
import renderPodium from './components/renderPodium';
import renderReviewHeader from './components/renderReviewHeader';

export default function componentResolver(type, data) {
  switch (type) {
    case 'feature':
      return renderFeature(data);
    case 'action button':
      return renderActionButton(data);
    case 'podium':
      return renderPodium(data);
    case 'HTML':
      return data.content;
    case 'review header':
      return renderReviewHeader(data);
    default:
      return '';
  }
}
