const renderActionButton = ({ text, link, sponsored }) => {
  return `<div class="action-wrapper"><a href="${link}" ${
    sponsored && 'rel="sponsored"'
  } target="_blank" class="action-button">${text}</a></div>`;
};

export default renderActionButton;
