const renderPodium = ({
  left_header,
  main_header,
  right_header,
  main_image,
  main_title,
  main_text,
  main_buy_link,
  left_title,
  left_buy_link,
  left_image,
  right_title,
  right_buy_link,
  right_image,
}) => {
  const content = `
    <div class="podium-wrapper">
      <div class="podium-12">
        <div class="podium-main podium">
            <div class="podium-header">
              ${main_header}
              <div class="rating">${starSVG}${starSVG}${starSVG}</div>
            </div>
            <div class="podium-content">
              <div class="podium-image">
                <a href="${main_buy_link}" target="_blank" rel="sponsored"><img alt="${main_title}" src="${main_image.filename}/m/" /></a>
              </div>
              <div class="podium-text">
                <h2>${main_title}</h2>
                <p>${main_text}</p>
                <a class="action-button" href="${main_buy_link}" rel="sponsored" target="_blank">Buy now</a>
              </div>
            </div>
        </div>
        <div class="podium-left podium">
          <div class="podium-header">${left_header}<div class="rating">${starSVG}${starSVG}</div></div>
          <div class="podium-content podium-side">
            <h2>${left_title}</h2>
            <div class="podium-image">
              <a href="${left_buy_link}" target="_blank" rel="sponsored"><img alt="${left_title}" src="${left_image.filename}/m/" /></a>
            </div>
            <a class="action-button" href="${left_buy_link}" rel="sponsored" target="_blank">Buy now</a>
          </div>
        </div>
      </div>
      <div class="podium-right podium">
          <div class="podium-header">${right_header}<div class="rating">${awardSVG}</div></div>
          <div class="podium-content podium-side">
            <h2>${right_title}</h2>
            <div class="podium-image">
              <a href="${right_buy_link}" target="_blank" rel="sponsored"><img alt="${right_title}" src="${right_image.filename}/m/" /></a>
            </div>
            <a class="action-button" href="${right_buy_link}" rel="sponsored" target="_blank">Buy now</a>
          </div>          
      </div>
    </div>
  `;

  return content;
};

export default renderPodium;

const starSVG = `<svg fill="#fff" width="25px" height="25px" version="1.1" id="star" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 329.942 329.942" style="enable-background:new 0 0 329.942 329.942;" xml:space="preserve">
<path id="XMLID_16_" d="M329.208,126.666c-1.765-5.431-6.459-9.389-12.109-10.209l-95.822-13.922l-42.854-86.837
	c-2.527-5.12-7.742-8.362-13.451-8.362c-5.71,0-10.925,3.242-13.451,8.362l-42.851,86.836l-95.825,13.922
	c-5.65,0.821-10.345,4.779-12.109,10.209c-1.764,5.431-0.293,11.392,3.796,15.377l69.339,67.582L57.496,305.07
	c-0.965,5.628,1.348,11.315,5.967,14.671c2.613,1.899,5.708,2.865,8.818,2.865c2.387,0,4.784-0.569,6.979-1.723l85.711-45.059
	l85.71,45.059c2.208,1.161,4.626,1.714,7.021,1.723c8.275-0.012,14.979-6.723,14.979-15c0-1.152-0.13-2.275-0.376-3.352
	l-16.233-94.629l69.339-67.583C329.501,138.057,330.972,132.096,329.208,126.666z"/></svg>`;

const awardSVG = `
<svg width="25px" height="25px" fill="#fff" width="512px" height="512px" viewBox="-64 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M97.12 362.63c-8.69-8.69-4.16-6.24-25.12-11.85-9.51-2.55-17.87-7.45-25.43-13.32L1.2 448.7c-4.39 10.77 3.81 22.47 15.43 22.03l52.69-2.01L105.56 507c8 8.44 22.04 5.81 26.43-4.96l52.05-127.62c-10.84 6.04-22.87 9.58-35.31 9.58-19.5 0-37.82-7.59-51.61-21.37zM382.8 448.7l-45.37-111.24c-7.56 5.88-15.92 10.77-25.43 13.32-21.07 5.64-16.45 3.18-25.12 11.85-13.79 13.78-32.12 21.37-51.62 21.37-12.44 0-24.47-3.55-35.31-9.58L252 502.04c4.39 10.77 18.44 13.4 26.43 4.96l36.25-38.28 52.69 2.01c11.62.44 19.82-11.27 15.43-22.03zM263 340c15.28-15.55 17.03-14.21 38.79-20.14 13.89-3.79 24.75-14.84 28.47-28.98 7.48-28.4 5.54-24.97 25.95-45.75 10.17-10.35 14.14-25.44 10.42-39.58-7.47-28.38-7.48-24.42 0-52.83 3.72-14.14-.25-29.23-10.42-39.58-20.41-20.78-18.47-17.36-25.95-45.75-3.72-14.14-14.58-25.19-28.47-28.98-27.88-7.61-24.52-5.62-44.95-26.41-10.17-10.35-25-14.4-38.89-10.61-27.87 7.6-23.98 7.61-51.9 0-13.89-3.79-28.72.25-38.89 10.61-20.41 20.78-17.05 18.8-44.94 26.41-13.89 3.79-24.75 14.84-28.47 28.98-7.47 28.39-5.54 24.97-25.95 45.75-10.17 10.35-14.15 25.44-10.42 39.58 7.47 28.36 7.48 24.4 0 52.82-3.72 14.14.25 29.23 10.42 39.59 20.41 20.78 18.47 17.35 25.95 45.75 3.72 14.14 14.58 25.19 28.47 28.98C104.6 325.96 106.27 325 121 340c13.23 13.47 33.84 15.88 49.74 5.82a39.676 39.676 0 0 1 42.53 0c15.89 10.06 36.5 7.65 49.73-5.82zM97.66 175.96c0-53.03 42.24-96.02 94.34-96.02s94.34 42.99 94.34 96.02-42.24 96.02-94.34 96.02-94.34-42.99-94.34-96.02z"/></svg>`;
