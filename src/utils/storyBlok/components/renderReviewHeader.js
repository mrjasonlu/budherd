import dropIcon from './icons/drop';
import flowerIcon from './icons/flower';
import conductionIcon from './icons/conduction';
import convectionIcon from './icons/convection';
import portableIcon from './icons/portable';
import desktopIcon from './icons/desktop';

const renderReviewHeader = ({
  cost,
  heating,
  portability,
  temperature,
  type,
  image,
  summary,
  link,
  title,
  small_format,
}) => {
  let typeIcon;
  if (type.length > 1) {
    typeIcon = `${flowerIcon} Flower /&nbsp; ${dropIcon} Concentrates`;
  } else if (type.length && type[0] === 'Flower') {
    typeIcon = flowerIcon + ' Flower';
  } else if (type.length && type[0] === 'Concentrates') {
    typeIcon = dropIcon + ' Concentrates';
  }

  let heatIcon;
  if (heating === 'Hybrid') {
    heatIcon = `${convectionIcon}&nbsp;Convection /&nbsp;${conductionIcon} Conduction`;
  } else if (heating === 'Convection') {
    heatIcon = `${convectionIcon}&nbsp;Convection`;
  } else if (heating === 'Conduction') {
    heatIcon = `${conductionIcon} Conduction`;
  }

  let portabilityIcon;
  if (portability === 'Portable') {
    portabilityIcon = `${portableIcon}&nbsp;Portable`;
  } else {
    portabilityIcon = `${desktopIcon}&nbsp;Desktop`;
  }

  const content = `
    <div class="review-header ${small_format && 'small-format'}">
        <div class="review-header-image">
          <img src="${image.filename}/m/" alt="${image.title}">
        </div>
        <div class="review-header-content">
          <h2 class="review-header-title">${title}</h2>
          <div class="review-header-card">
            <div class="card-row">
              <span>Type: </span>
              <div class="card-row-content">
                <div class="card-row-icons">${typeIcon}</div>
              </div>
            </div>
            <div class="card-row"><span>Heating: </span><div class="card-row-icons">${heatIcon}</div></div>
            <div class="card-row"><span>Portability: </span><div class="card-row-icons">${portabilityIcon}</div></div>
            <div class="card-row"><span>Cost: </span>${cost}</div>
            <div class="card-row"><span>Temperature: </span>${temperature}</div>
            <div class="bui-flex bui-align-center bui-justify-center bui-margin-bottom"><a class="action-button" href="/link/${link}" rel="sponsored" target="_blank">Check Price</a></div>
          </div>
        </div>
    </div>
  `;

  return content;
};

export default renderReviewHeader;
