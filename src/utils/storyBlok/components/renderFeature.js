import StoryBlok from '~/utils/storyBlok';

const setLinkSponsoredAttr = (content) => {
  if (content.type === 'text' && content.marks) {
    const linkMarkIndex = content.marks.findIndex(
      (mark) => mark.type === 'link'
    );
    if (linkMarkIndex !== -1) {
      content.marks[linkMarkIndex].attrs.target = '_self';
    }
  }
  if (content.content && content.content.length) {
    setLinkSponsoredAttr(content.content);
  }
};

const renderFeature = ({
  text,
  image,
  layout,
  altText,
  disableSrcSet,
  link,
  copyright,
}) => {
  let content;

  let srcSetAttr = '';
  if (!disableSrcSet) {
    srcSetAttr = `srcSet="${StoryBlok.getSrcSet(
      image
    )}" sizes="(min-width: 768px) ${
      layout === 'feature-full' ? '40vw' : '20vw'
    }, 100vw"`;
  }

  let srcSetAttrJpg = '';
  if (!disableSrcSet) {
    srcSetAttrJpg = `srcSet="${StoryBlok.getSrcSet(image, false)}"`;
  }

  if (text) {
    content = `<blockquote class='${layout}'>"${text}"</blockquote>`;
  } else {
    if (copyright) {
      content = `<figure class='${layout} ${
        disableSrcSet ? 'disableSrcSet' : ''
      }'>        <picture>
      <source
        ${srcSetAttr}
        type="image/webp"
      />
      <source
        ${srcSetAttrJpg}
        type="image/jpeg"
      />
      <img src="${image}" alt="${altText}" title="${altText}" sizes="(min-width: 768px) ${
        layout === 'feature-full' ? '40vw' : '20vw'
      }, 100vw" />
    </picture><div class="copyright">Image by ${copyright}</div></figure>`;
    } else {
      content = `<figure class='${layout} ${
        disableSrcSet ? 'disableSrcSet' : ''
      }'>
        <picture>
          <source
            ${srcSetAttr}
            type="image/webp"
          />
          <source
            ${srcSetAttrJpg}
            type="image/jpeg"
          />
          <img src="${image}" alt="${altText}" title="${altText}" sizes="(min-width: 768px) ${
        layout === 'feature-full' ? '40vw' : '20vw'
      }, 100vw" />
        </picture>
        </figure>`;
    }
  }
  if (link) {
    content = `<a href=${link} target="_blank">${content}</a>`;
  }
  return content;
};

export default renderFeature;
