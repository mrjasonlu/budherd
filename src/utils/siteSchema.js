export function makeOrgSchema() {
  return {
    '@context': 'http://schema.org',
    '@type': 'Organization',
    name: 'BudHerd',
    url: 'https://www.budherd.com.au/',
    logo: 'https://budherd.com.au/static/favicon/favicon-196.png',
    // sameAs: ['http://www.facebook.com/budherd'],
    founder: [
      {
        '@type': 'Person',
        name: 'Jason Lu',
        sameAs: 'www.linkedin.com/in/mrjlu',
      },
    ],
  };
}

export function makeSiteSchema() {
  return {
    '@context': 'http://schema.org/',
    '@type': 'WebSite',
    name: 'BudHerd',
    url: 'https://www.budherd.com.au/',
    potentialAction: {
      '@type': 'SearchAction',
      target:
        'https://www.budherd.com.au/search?q={search_term_string}',
      'query-input': 'required name=search_term_string',
    },
  };
}
