export const addLineBreaks = (string) =>
  string.split('\n').map((text, index) => (
    <React.Fragment key={`${text}-${index}`}>
      {text}
      <br />
    </React.Fragment>
  ));

export const getInitials = (name = '') => {
  const nameSplit = name.split(' ');
  return nameSplit.length > 1
    ? nameSplit[0].charAt(0).toUpperCase() +
        nameSplit[nameSplit.length - 1].charAt(0).toUpperCase()
    : nameSplit[0].charAt(0).toUpperCase();
};
