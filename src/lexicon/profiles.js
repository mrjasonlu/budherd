const profiles = {
  jasonLu: {
    name: 'Jason Lu | BudHerd',
    image: '/static/images/profiles/jason-lu.jpg',
    profile:
      'Jason is one of the lead editors and founder of BudHerd. He spends his days' +
      ' writing, designing, developing and researching all things cannabis. Jason is pa' +
      'ssionate about destigmatising and educating Australians on the therapeutic and r' +
      'ecreational values of cannabis.',
  },
  rhondaTeoh: {
    name: 'Rhonda Teoh | BudHerd',
    image:
      'https://a.storyblok.com/f/81752/150x150/d98648a61f/rhonda-teoh-150x150.webp',
    profile:
      'Rhonda is a pro-cannabis writer based in Melbourne. She is focused on researching and spreading the word on how cannabis can benefit the lives of everyday Australians.',
  },
  harrisonBlack: {
    name: 'Harrison Black | BudHerd',
    image: '',
    profile:
      'Harrison is a freelance writer who is passionate about the legalisation of recreational cannabis.' +
      ' You can find him cycling or surfing the days away in coastal Victoria when he is away from the keyboard.',
  },
};

export default profiles;
