import React from 'react';
import { Layout } from 'antd';
import Header from '~/components/header';
import Footer from '~/components/footer';

export default ({ children, hideHeader, hideFooter }) => {
  return (
    <Layout>
      {!hideHeader ? <Header /> : null}
      {children}
      {!hideFooter ? <Footer /> : null}
    </Layout>
  );
};
