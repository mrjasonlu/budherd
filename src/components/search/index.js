import { Button, Modal, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import Router from 'next/router';
import styles from './styles.less';

const SearchInput = Input.Search;

export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.searchInput = React.createRef();
    this.state = {
      showSearch: false,
    };
  }

  handleClick = (e) => {
    this.setState({
      showSearch: !this.state.showSearch,
    });
  };

  handleSearch = (value) => {
    Router.push({
      pathname: '/search',
      query: {
        q: value,
      },
    });
    this.setState({ showSearch: false });
  };

  handleSearchEnter = (e) => {
    e.preventDefault();
    this.handleSearch(this.searchInput.current.input.input.value);
  };

  componentDidUpdate() {
    if (this.state.showSearch) {
      setTimeout(() => {
        this.searchInput.current.input.state.value = '';
        this.searchInput.current.input.focus();
      }, 0);
    }
  }

  render() {
    return (
      <div className={styles.search}>
        <Button
          type="link"
          shape="circle"
          onClick={this.handleClick}
          icon={<SearchOutlined />}
        />
        <Modal
          centered
          footer={null}
          maskStyle={{
            background: 'rgba(0,0,0,0.7)',
          }}
          bodyStyle={{
            padding: '55px',
          }}
          visible={this.state.showSearch}
          onCancel={() => this.setState({ showSearch: false })}>
          <SearchInput
            ref={this.searchInput}
            placeholder="Search BudHerd"
            onPressEnter={this.handleSearchEnter}
            onSearch={this.handleSearch}
          />
        </Modal>
      </div>
    );
  }
}
