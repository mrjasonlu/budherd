import { Form, Button, Select } from 'antd';
import Router from 'next/router';
import styles from './styles.less';
import cn from 'classnames';

const { Option } = Select;

const layout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 18,
  },
};

const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

export default ({ path, small, selected, submitOnSelect }) => {
  const [form] = Form.useForm();

  const onFinish = (value = {}) => {
    const { State } = value;
    if (State) {
      Router.push({ pathname: `${path}${State}` });
    }
  };

  const onSelect = (value) => {
    if (submitOnSelect) {
      onFinish({ State: value });
    }
  };

  return (
    <div
      className={cn({
        [styles.smallForm]: small,
        [styles.submitOnSelect]: submitOnSelect,
      })}>
      <Form
        layout="inline"
        form={form}
        name="control-ref"
        size="large"
        onFinish={onFinish}
        initialValues={{ State: selected }}
        className={styles.form}>
        <Form.Item
          name="State"
          label="State"
          className={styles.input}>
          <Select
            placeholder="Select State / Territory"
            onChange={onSelect}>
            <Option value="act">Australian Capital Territory</Option>
            <Option value="nsw">New South Wales</Option>
            <Option value="nt">Northern Territory</Option>
            <Option value="qld">Queensland</Option>
            <Option value="sa">South Australia</Option>
            <Option value="tas">Tasmania</Option>
            <Option value="vic">Victoria</Option>
            <Option value="wa">Western Australia</Option>
          </Select>
        </Form.Item>
        {!submitOnSelect ? (
          <Form.Item className={styles.submit}>
            <Button type="primary" htmlType="submit" size="large">
              Go
            </Button>
          </Form.Item>
        ) : null}
      </Form>
    </div>
  );
};
