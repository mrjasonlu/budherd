import { Row, Col, Avatar } from 'antd';
import styles from './styles.less';
import { getInitials } from '~/utils/helpers';
import Link from 'next/link';
import {
  createRadarChart,
  createISChart,
  strainColors,
} from '~/containers/strain/strainHelper';

export default ({
  strains,
  count,
  grid = { span: 8, xs: 24, sm: 24, md: 12, lg: 8, xxl: 6 },
}) => {
  const strainList = [];
  for (const i in strains) {
    if (i > count - 1) {
      break;
    }
    const listItem = strains[i];
    const {
      name,
      slug,
      category,
      indicaPct,
      sativaPct,
      cannabinoids: { thc, cbd },
    } = listItem;

    const radarSettings = {
      width: 190,
      height: 190,
      fontSize: 12,
      pointRadius: 5,
    };

    const isSettings = {
      width: 200,
      height: 5,
      barThickness: 2,
      paddingRight: 0,
      hideDataLabel: true,
    };

    const radar = createRadarChart(listItem, radarSettings);
    const isChart = createISChart(indicaPct, sativaPct, isSettings);
    strainList.push(
      <Col key={slug} {...grid}>
        <Link href="/strains/[slug]" as={`/strains/${slug}`}>
          <a className={styles.listItem}>
            <div className={styles.header}>
              <Avatar
                shape="square"
                style={{
                  backgroundColor: category
                    ? strainColors[category].line
                    : '#eee',
                  verticalAlign: 'middle',
                }}
                className={styles.cardAvatar}>
                {getInitials(category)}
              </Avatar>
              <h3
                style={{
                  borderBottom: `3px solid ${
                    category ? strainColors[category].line : '#eee'
                  }`,
                }}>
                <span>{name}</span>
              </h3>
            </div>
            <div className={styles.isInfo}>
              <div>
                THC <span>{thc.avg ? thc.avg : '< 1'}%</span>
              </div>
              <div className="bui-flex-right">
                CBD <span>{cbd.avg ? cbd.avg : '< 1'}%</span>
              </div>
            </div>
            <div className={styles.radarWrapper}>{radar}</div>
            {isChart}
          </a>
        </Link>
      </Col>
    );
  }

  return (
    <Row
      className={styles.listRow}
      gutter={[24, 36]}
      justify="left"
      type="flex">
      {strainList}
    </Row>
  );
};
