import {
  Row,
  Col,
  Avatar,
  Pagination,
  Select,
  Form
} from 'antd';
import {useRouter} from 'next/router';
import styles from './styles.less';
import StrainGrid from '~/components/strainGrid';

const {Option} = Select;

export default({strains, count, context, total, hideSort}) => {
  const Router = useRouter();
  const {query, pathname} = context;
  const {_sort, _order} = query;

  const handlePagination = (page) => {
    const newQuery = {
      ...query,
      _page: page
    };
    Router.push({pathname, query: newQuery});
  };

  const handleSort = (sort) => {
    let _sort = 'popularity';
    let _order;
    if (sort === 'AZ') {
      _sort = 'name';
      _order = 'asc';
    } else if (sort === 'ZA') {
      _sort = 'name';
      _order = 'desc';
    } else if (sort === 'popularity') {
      _sort = 'popularity';
      _order = 'asc';
    }
    const newQuery = {
      ...query,
      _sort,
      _order
    };
    Router.push({pathname: '/strains', query: newQuery});
  };

  let currentSort;
  if (_sort === 'name') {
    if (_order === 'desc') {
      currentSort = 'ZA';
    } else {
      currentSort = 'AZ';
    }
  } else {
    currentSort = 'popularity';
  }

  const sortInput = !hideSort
    ? (
      <Form className={styles.sortInput}>
        <Form.Item label="Sort by" name="sort">
          <Select defaultValue={currentSort} onChange={handleSort}>
            <Option value="popularity">Popularity</Option>
            <Option value="AZ">A-Z</Option>
            <Option value="ZA">Z-A</Option>
          </Select>
        </Form.Item>
      </Form>
    )
    : null;

  return (
    <Row justify="center">
      <Col span={24} justify="center">
        {sortInput}
        <StrainGrid strains={strains} count={count}/> {strains && strains.length
          ? (
            <div className={styles.pagination}>
              <Pagination
                pageSize={24}
                defaultCurrent={query._page}
                total={total}
                showSizeChanger={false}
                onChange={handlePagination}/>
            </div>
          )
          : null}
      </Col>
    </Row>
  );
};
