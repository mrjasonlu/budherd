import { List, Card, Row, Col, Button, Avatar } from 'antd';
// import Map from '~/components/map';
import Filters from './filters';
import {
  EnvironmentFilled,
  PhoneFilled,
  LinkOutlined,
  MailOutlined,
  UserOutlined,
} from '@ant-design/icons';
import cn from 'classnames';
import { getInitials } from '~/utils/helpers';
import styles from './styles.less';

export default ({
  list,
  categorise,
  categorySort,
  filters,
  title,
  path,
  profile,
  emptyContent,
  footerContent,
  state,
  subCategory,
  filterHeader,
}) => {
  const doctorDetails = (item) => {
    const {
      image,
      category,
      name,
      location,
      national,
      service,
      position,
      contact,
      features,
    } = item;
    let address = null;

    if (national) {
      address = 'Australia Wide';
    } else if (
      category === 'clinic' &&
      Object.keys(service).length > 1
    ) {
      address = Object.keys(service)
        .map((s) => s.toUpperCase())
        .join(', ');
    } else if (location && location[0] && location[1]) {
      if (location[0].state === location[1].state) {
        address = `${location[0].suburb} & ${location[1].suburb}, ${location[0].state}`;
      } else {
        address = `${location[0].suburb}, ${location[0].state} & ${location[1].suburb}, ${location[1].state}`;
      }
    } else if (location && location[0]) {
      address = (
        <a
          href={`http://maps.google.com/maps?q=${encodeURIComponent(
            `${location[0].street}, ${location[0].suburb}, ${location[0].state}, ${location[0].postcode}, Australia`
          )}`}
          target="_blank">{`${location[0].street}, ${location[0].suburb}, ${location[0].state}`}</a>
      );
    } else {
      address = Object.keys(service)
        .map((s) => s.toUpperCase())
        .join(', ');
    }

    const cardTitle = position ? (
      <div>
        {name}
        <span>{position}</span>
      </div>
    ) : (
      name
    );

    const featureTags =
      features && features.length
        ? features.map((feature) => (
            <span key={feature}>{feature}</span>
          ))
        : null;
    return (
      <List.Item className={styles.itemWrapper}>
        <Card className={styles.listItem} title={cardTitle}>
          <div className={styles.itemContent}>
            <div className={styles.image}>
              <div
                className={cn(styles.avatar, {
                  [styles.profile]: profile(item),
                })}>
                {image ? (
                  <img src={image} />
                ) : profile(item) ? (
                  <Avatar size={64} icon={<UserOutlined />} />
                ) : (
                  <Avatar
                    shape="square"
                    style={{ backgroundColor: '#ccc' }}
                    size={64}>
                    {getInitials(name)}
                  </Avatar>
                )}
              </div>
            </div>
            <div className={styles.details}>
              <div className={styles.contact}>
                {address ? (
                  <div className={styles.entry}>
                    <EnvironmentFilled />
                    <div>{address}</div>
                  </div>
                ) : null}
                {contact && contact.phone ? (
                  <div className={styles.entry}>
                    <PhoneFilled />
                    <a href={`tel:${contact.phone}`} target="_blank">
                      {contact.phone}
                    </a>
                  </div>
                ) : null}
              </div>
              {featureTags ? (
                <div className={styles.features}>{featureTags}</div>
              ) : null}
            </div>
          </div>
          <div>
            {contact && (contact.website || contact.email) ? (
              <div className={cn(styles.entry, styles.links)}>
                {contact.website ? (
                  <div className="bui-margin-left">
                    <Button
                      type="primary"
                      icon={<LinkOutlined />}
                      href={contact.website}
                      target="_blank"
                      rel="nofollow">
                      Website
                    </Button>
                  </div>
                ) : null}
                {contact.email ? (
                  <div>
                    <Button
                      icon={<MailOutlined />}
                      type="primary"
                      href={`mailto:${contact.email}`}
                      target="_blank"
                      rel="nofollow">
                      Email
                    </Button>
                  </div>
                ) : null}
              </div>
            ) : null}
          </div>
        </Card>
      </List.Item>
    );
  };

  let listContent = null;
  if (!list || !list.length) {
    listContent = emptyContent;
  } else {
    if (categorise) {
      let listing = {};
      list.forEach((l) => {
        if (!Array.isArray(listing[l.category])) {
          listing[l.category] = [];
        }
        if (l.service[state] === true) {
          listing[l.category].push(l);
        } else if (subCategory(l.service[state])) {
          const subCatTitle = subCategory(l.service[state]);
          if (!Array.isArray(listing[subCatTitle])) {
            listing[subCatTitle] = [];
          }
          listing[subCatTitle].push(l);
        }
      });
      const categories = Object.keys(listing);
      if (categorySort) {
        categories.sort(categorySort);
      }
      const categoryList = categories.map((category) => {
        if (listing[category] && listing[category].length) {
          return (
            <div key={categorise[category]}>
              <h2>{categorise[category]}</h2>
              <List
                grid={{
                  gutter: 16,
                  xs: 1,
                  sm: 1,
                  md: 1,
                  lg: 2,
                }}
                dataSource={listing[category]}
                renderItem={doctorDetails}
              />
            </div>
          );
        }
        return null;
      });
      listContent = (
        <div>{categoryList.filter((c) => c).reverse()}</div>
      );
    } else {
      listContent = (
        <List
          grid={{
            gutter: 16,
            xs: 1,
            sm: 1,
            md: 1,
            lg: 2,
          }}
          dataSource={list}
          renderItem={doctorDetails}
        />
      );
    }
  }

  return (
    <Row justify="space-around" type="flex">
      <Col span={7} xs={23} sm={23} md={7}>
        <div className={styles.filterContainer}>
          <Filters
            filters={filters}
            path={path}
            header={filterHeader}
          />
        </div>
      </Col>
      <Col
        span={17}
        xs={23}
        sm={23}
        md={17}
        className={styles.listContainer}>
        <h1>{title}</h1>
        {listContent}
        {footerContent}
      </Col>
    </Row>
  );
};
