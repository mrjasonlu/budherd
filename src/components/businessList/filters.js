import {Checkbox} from 'antd';
import Router from 'next/router';
import styles from './styles.less';

export default({filters, path, header}) => {
  const onChange = (checkedValues, name) => {
    let query = {};
    checkedValues.length
      ? (query[name] = checkedValues.join(','))
      : {};
    Router.push({pathname: path.full, query});
  };

  let filterContent;
  if (filters && filters.length) {
    filterContent = filters.map(({labels, name}) => {
      const options = labels
        ? Object
          .keys(labels)
          .map((f) => ({label: labels[f], value: f}))
        : [];
      const defaultChecked = path.query[name]
        ? path
          .query[name]
          .split(',')
        : [];
      return (<Checkbox.Group
        key={name}
        options={options}
        onChange={(checkedValues) => onChange(checkedValues, name)}
        defaultValue={defaultChecked}
        className={styles.filters}/>);
    });
  }

  return (
    <div className="bui-margin-bottom-xlarge">
      {header}
      <h3>Filters</h3>
      {filterContent}
    </div>
  );
};
