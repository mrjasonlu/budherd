import { Row, Col, Tag } from 'antd';
import styles from './styles.less';
import cn from 'classnames';
import Link from 'next/link';
import StoryBlok from '~/utils/storyBlok';

export default ({
  posts,
  count,
  small,
  colSpan = 8,
  getPath = () => 'articles',
}) => {
  const postList = [];
  for (const i in posts) {
    if (i > count - 1) {
      break;
    }
    const listItem = posts[i];
    const path = getPath(listItem);
    const {
      content: { title, mainImage, thumbnail },
      tag_list,
      slug,
    } = listItem;
    const mainImageUrl =
      mainImage && typeof mainImage === 'string'
        ? mainImage
        : mainImage
        ? mainImage.filename
        : null;
    let listImage;
    if (thumbnail) {
      listImage = (
        <picture>
          <source
            srcSet={StoryBlok.getSrcSet(thumbnail)}
            type="image/webp"
          />
          <source
            srcSet={StoryBlok.getSrcSet(thumbnail, false)}
            type="image/jpeg"
          />
          <img
            src={thumbnail}
            alt={`article image for ${title}`}
            sizes="(min-width: 768px) 20vw, 100vw"
          />
        </picture>
      );
    } else if (mainImage) {
      listImage = (
        <picture>
          <source
            srcSet={StoryBlok.getSrcSet(mainImageUrl)}
            type="image/webp"
          />
          <source
            srcSet={StoryBlok.getSrcSet(mainImageUrl, false)}
            type="image/jpeg"
          />
          <img
            src={mainImageUrl}
            alt={`article image for ${title}`}
            sizes="(min-width: 768px) 20vw, 100vw"
          />
        </picture>
      );
    }
    const tagList = tag_list.map((tag) => (
      <Tag key={tag} color="#2e8508">
        {tag}
      </Tag>
    ));

    postList.push(
      <Col key={slug} span={colSpan} xs={24} sm={24} md={colSpan}>
        <Link href={`/${path}/[slug]`} as={`/${path}/${slug}`}>
          <a
            className={cn(styles.listItem, {
              [styles.smallList]: small,
            })}>
            <div className={styles.imageWrapper}>{listImage}</div>
            {tagList.length ? (
              <div className={styles.tags}>{tagList}</div>
            ) : null}
            <h3>{title}</h3>
          </a>
        </Link>
      </Col>
    );
  }
  return (
    <Row justify="center">
      <Col span={24} justify="center">
        <Row
          className={styles.listRow}
          gutter={[24, 24]}
          justify="left"
          type="flex">
          {postList}
        </Row>
      </Col>
    </Row>
  );
};
