import { Breadcrumb, Col } from 'antd';
import { layoutSizes } from '~/config';
import Link from 'next/link';

export default ({ crumbs }) => {
  const links =
    crumbs && crumbs.length
      ? crumbs.map((c) => {
          if (c.path) {
            return (
              <Breadcrumb.Item key={c.name}>
                <Link href={c.path}>
                  <a>{c.name}</a>
                </Link>
              </Breadcrumb.Item>
            );
          } else {
            return (
              <Breadcrumb.Item key={c.name}>{c.name}</Breadcrumb.Item>
            );
          }
        })
      : [];
  return (
    <Col {...layoutSizes}>
      <Breadcrumb className="bui-margin-bottom-xlarge bui-padding-left-large">
        <Breadcrumb.Item key="home">
          <Link href="/">
            <a>Home</a>
          </Link>
        </Breadcrumb.Item>
        {links}
      </Breadcrumb>
    </Col>
  );
};
