import { List, Card } from 'antd';
import { Avatar } from 'antd';
import profiles from '~/lexicon/profiles';
import styles from './styles.less';

export default ({ coAuthor, hideDefault, defaultAuthorName }) => {
  const defaultAuthor =
    defaultAuthorName && profiles[defaultAuthorName]
      ? profiles[defaultAuthorName]
      : profiles.jasonLu;
  let list = [];
  if (coAuthor && coAuthor.name) {
    list.push(coAuthor);
    if (!hideDefault) {
      list.push(defaultAuthor);
    }
  } else {
    list.push(defaultAuthor);
  }

  const gridLength = list.length > 1 ? 2 : 1;
  return (
    <div>
      <List
        id="authorlist"
        className="bui-margin-top-xxlarge"
        grid={{
          gutter: 16,
          xs: 1,
          sm: 1,
          md: gridLength,
        }}
        dataSource={list}
        renderItem={(item) => (
          <List.Item>
            <div className="bui-padding">
              <div className={styles.authorProfile}>
                <Avatar
                  className={styles.profileImage}
                  src={item.image}
                  size={64}
                  alt={item.name}
                />
                <div className={styles.profileContent}>
                  <h4>{item.name}</h4>
                  <div>{item.profile}</div>
                </div>
              </div>
            </div>
          </List.Item>
        )}
      />
    </div>
  );
};
