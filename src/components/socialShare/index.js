import {
  EmailShareButton,
  FacebookShareButton,
  PinterestShareButton,
  RedditShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  EmailIcon,
  FacebookIcon,
  TwitterIcon,
  PinterestIcon,
  WhatsappIcon,
  RedditIcon,
} from 'react-share';
import styles from './styles.less';

export default class SocialShare extends React.Component {
  render() {
    const { title, image, description, url, hideText } = this.props;
    return (
      <div className={styles.socialShare}>
        {hideText ? null : <span>Share:</span>}
        <FacebookShareButton
          url={url}
          quote={title}
          hashtag="budherd">
          <FacebookIcon
            round
            size={32}
            bgStyle={{ fill: '#888888' }}
          />
        </FacebookShareButton>
        <TwitterShareButton
          url={url}
          via="budherd"
          hashtags={['budherd']}
          title={title}>
          <TwitterIcon
            round
            size={32}
            bgStyle={{ fill: '#888888' }}
          />
        </TwitterShareButton>
        <EmailShareButton
          url={url}
          subject={title}
          body={description}>
          <EmailIcon round size={32} bgStyle={{ fill: '#888888' }} />
        </EmailShareButton>
        <PinterestShareButton
          url={url}
          media={image}
          description={description}>
          <PinterestIcon
            round
            size={32}
            bgStyle={{ fill: '#888888' }}
          />
        </PinterestShareButton>
        {/* <WhatsappShareButton url={url} title={title}>
          <WhatsappIcon
            round
            size={32}
            bgStyle={{ fill: '#888888' }}
          />
        </WhatsappShareButton> */}
        {/* <RedditShareButton url={url} title={title}>
          <RedditIcon round size={32} bgStyle={{ fill: '#888888' }} />
        </RedditShareButton>         */}
      </div>
    );
  }
}
