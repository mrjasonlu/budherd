import { Row, Col, Layout, Drawer, Button } from 'antd';
import { MenuOutlined } from '@ant-design/icons';
import LogoSvg from './bhLogo.svg';
import Menu from '~/components/menu';
import Search from '~/components/search';
import { layoutSizes } from '~/config';
const { Header } = Layout;
import styles from './styles.less';

export default () => {
  const [showDrawer, setShowDrawer] = React.useState(false);
  return (
    <Header className={styles.header}>
      <Row justify="space-around" type="flex">
        <Col {...layoutSizes}>
          <Row type="flex">
            <Col span={5} md={8} sm={22} xs={22}>
              <div className="bui-flex bui-align-center bui-padding-top">
                <Button
                  type="link"
                  size="large"
                  onClick={() => setShowDrawer(true)}
                  className={styles.menuIcon}
                  icon={<MenuOutlined />}>
                  <span className="bui-v-hidden">Menu</span>
                </Button>
                <a className={styles.headerLogo} href="/">
                  <span>BUDHERD</span>
                  <LogoSvg width="20" height="20" />
                </a>
              </div>
            </Col>
            <Col span={19} lg={15} md={0} xs={0}>
              <div className="bui-flex-row-reverse bui-flex">
                <Menu />
              </div>
            </Col>
            <Col span={1} lg={1} md={15} xs={1}>
              <Search />
            </Col>
          </Row>
        </Col>
      </Row>
      <Drawer
        title="BUDHERD"
        placement="left"
        closable={false}
        className={styles.sideMenu}
        onClose={() => setShowDrawer(false)}
        visible={showDrawer}>
        <Menu mode="vertical" />
      </Drawer>
    </Header>
  );
};
