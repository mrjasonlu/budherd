import { Menu } from 'antd';
import Link from 'next/link';
import styles from './styles.less';

export default class TopMenu extends React.Component {
  render() {
    const { mode = 'horizontal' } = this.props;
    return (
      <div className={styles.mainMenu}>
        <Menu mode={mode}>
          <Menu.Item key="articles">
            <Link href="/articles">
              <a>Learn</a>
            </Link>
          </Menu.Item>
          <Menu.Item key="vapes">
            <Link href="/vaporizers">
              <a>Vaping</a>
            </Link>
          </Menu.Item>
          <Menu.Item key="strains">
            <Link href="/strains">
              <a>Strains</a>
            </Link>
          </Menu.Item>
          <Menu.Item key="doctors">
            <Link href="/nearme/doctors">
              <a>Find Doctors</a>
            </Link>
          </Menu.Item>
          <Menu.Item key="cbd">
            <Link
              href="/articles/[slug]"
              as="/articles/complete-guide-to-cbd-oil-in-australia">
              <a>CBD Oil</a>
            </Link>
          </Menu.Item>
        </Menu>
      </div>
    );
  }
}
