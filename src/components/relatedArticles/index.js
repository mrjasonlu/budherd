import { useEffect, useState } from 'react';
import { fetchPostsWithTags } from '~/resources';
import ArticleList from '~/components/articleList';
import { Defer } from 'react-progressive-loader';

export default ({ tags, slug }) => {
  const [relatedPosts, setRelatedPosts] = useState([]);
  const fetchRelatedPosts = async (tags, slug) => {
    const posts = await fetchPostsWithTags(tags, `posts/${slug}`);
    setRelatedPosts(posts);
  };
  useEffect(() => {
    fetchRelatedPosts(tags, slug);
  });

  if (!relatedPosts.length) {
    return null;
  }

  return (
    <div>
      <h4>Related articles</h4>
      <Defer
        render={() => (
          <ArticleList posts={relatedPosts} count={3} small />
        )}
        renderPlaceholder={() => null}
        loadOnScreen
      />
    </div>
  );
};
