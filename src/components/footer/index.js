import { Row, Col } from 'antd';
import Link from 'next/link';
import styles from './styles.less';

export default () => (
  <Row justify="space-around" type="flex" className={styles.footer}>
    <Col md={9} xs={20} className="bui-margin-bottom-large">
      <div>
        &copy;
        <span className={styles.copy}>BudHerd </span>
        {new Date().getFullYear()}
      </div>
    </Col>

    <Col md={5} xs={20} className="bui-margin-bottom-large">
      <h5>Explore Cannabis</h5>
      <ul>
        <li>
          <Link href="/articles">
            <a>Cannabis Education</a>
          </Link>
        </li>
        <li>
          <Link href="/vaporizers">
            <a>Dry Herb Vaporizers</a>
          </Link>
        </li>
        <li>
          <Link href="/strains">
            <a>Strain Explorer</a>
          </Link>
        </li>
      </ul>
    </Col>
    <Col md={5} xs={20} className="bui-margin-bottom-large">
      <h5>Medicinal Cannabis</h5>
      <ul>
        <li>
          <Link href="/nearme/doctors">
            <a>Find Medicinal Cannabis Doctors</a>
          </Link>
        </li>
        <li>
          <Link
            href="/articles/[slug]"
            as="/articles/complete-guide-to-cbd-oil-in-australia">
            <a>Complete Guide to CBD Oil in Australia</a>
          </Link>
        </li>
        <li>
          <Link
            href="/articles/[slug]"
            as="/articles/hemp-seed-oil-vs-hemp-oil-vs-cbd-oil">
            <a>Hemp Seed Oil vs CBD Oil</a>
          </Link>
        </li>
      </ul>
    </Col>
    <Col md={5} xs={20} className="bui-margin-bottom-large">
      <h5>BudHerd</h5>
      <ul>
        <li>
          <Link href="/about">
            <a>About Us</a>
          </Link>
        </li>
        <li>
          <Link href="/contact">
            <a>Contact</a>
          </Link>
        </li>
        <li>
          <Link href="/disclosure">
            <a>Disclosure</a>
          </Link>
        </li>
        <li>
          <Link href="/terms-of-use">
            <a>Terms and Conditions</a>
          </Link>
        </li>
        <li>
          <Link href="/privacy-policy">
            <a>Privacy Policy</a>
          </Link>
        </li>
      </ul>
    </Col>
  </Row>
);
