// import Marker from '~/components/map/marker';
// import GoogleMapReact from 'google-map-react';
// import customStyles from './theme';

// const mapDefault = {
//   center: { lat: -37.8274812, lng: 144.9352466 },
//   zoom: 11,
// };

// export default () => {
//   return (
//     <GoogleMapReact
//       bootstrapURLKeys={{
//         key: process.env.MAP_API_KEY,
//       }}
//       defaultCenter={mapDefault.center}
//       defaultZoom={mapDefault.zoom}
//       options={{ styles: customStyles, fullscreenControl: false }}>
//       <Marker lat={-37.8274812} lng={144.9352476} text="A" />
//     </GoogleMapReact>
//   );
// };
