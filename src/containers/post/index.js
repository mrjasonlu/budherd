import { useEffect } from 'react';
import { Row, Col, Layout, Tag, Card } from 'antd';
import * as tocbot from 'tocbot';
import PageLayout from '~/components/layout';
import styles from './styles.less';
import StoryBlok from '~/utils/storyBlok';
import { NextSeo, ArticleJsonLd } from 'next-seo';
import RelatedArticles from '~/components/relatedArticles';
import AuthorList from '~/components/authorList';
import Breadcrumb from '~/components/breadcrumb';
import SocialShare from '~/components/socialShare';
import { disclaimer } from '~/config/lexicon';
import { DateTime } from 'luxon';
const { Content } = Layout;

const tocOptions = {
  headingSelector: 'h2, h3',
  contentSelector: '.bui-content',
  tocSelector: '.toc',
  positionFixedSelector: '.article-toc',
  positionFixedClass: 'toc-position-fixed',
};

function addHeadingIds() {
  const hList = document.querySelectorAll(
    '.bui-content h2, .bui-content h3'
  );
  hList.forEach((h) => {
    let anchor = h.textContent
      .replace(/^[^.]+\./, '')
      .replace(/[^0-9a-zA-Z \-]/g, '')
      .replace(/\s+/g, '-')
      .toLowerCase();
    h.id = anchor;
  });
}

const Post = ({ data = {}, slug, type }) => {
  useEffect(() => {
    addHeadingIds();
    const tocDiv = document.querySelector('.article-toc');
    let rect = tocDiv.getBoundingClientRect();
    tocbot.init({
      ...tocOptions,
      positionFixedClass:
        data && data.content && data.content.infographicUrl
          ? 'none'
          : 'toc-position-fixed',
      fixedSidebarOffset: rect.top + (window ? window.scrollY : 0),
    });

    if (window.location.hash) {
      const el = document.querySelector(window.location.hash);
      if (el) {
        setTimeout(
          () =>
            el.scrollIntoView({
              behavior: 'smooth',
              block: 'center',
            }),
          0
        );
      }
    }
  });

  const {
    content: {
      title,
      subTitle,
      mainImage,
      thumbnail,
      coverImageFixedHeight,
      infographicUrl,
      intro,
      body,
      citations,
      seoTitle,
      seoDescription,
      coAuthorName,
      coAuthorSummary,
      coAuthorImage,
      hideDefaultAuthor,
      coAuthorIsTeam,
      defaultAuthor,
    },
    published_at,
    created_at,
    tag_list,
  } = data;

  const url = `https://budherd.com.au/articles/${slug}`;

  const tagList = tag_list.map((tag) => (
    <Tag key={tag} color="#2e8508">
      {tag}
    </Tag>
  ));

  const mainImageUrl =
    mainImage && typeof mainImage === 'string'
      ? mainImage
      : mainImage
      ? mainImage.filename
      : null;

  let imageUrl = infographicUrl
    ? `/static/images/infographic/${infographicUrl}`
    : mainImageUrl;

  const postImage = imageUrl ? (
    <figure
      className={`feature-full feature-main ${
        coverImageFixedHeight ? 'fixedHeight' : ''
      }`}>
      <picture>
        <source
          srcSet={
            infographicUrl ? '' : StoryBlok.getSrcSet(mainImageUrl)
          }
          type="image/webp"
        />
        <source
          srcSet={
            infographicUrl
              ? ''
              : StoryBlok.getSrcSet(mainImageUrl, false)
          }
          type="image/jpeg"
        />
        <img
          alt={`cover image for article ${title}`}
          src={imageUrl}
        />
      </picture>
      {mainImage.copyright ? (
        <div className="copyright">
          Image by {mainImage.copyright}
        </div>
      ) : null}
    </figure>
  ) : null;

  let shareInfographic = null;
  if (infographicUrl) {
    shareInfographic = (
      <div className="infographic-code">
        <p>Embed this infographic on your page:</p>
        <textarea
          readOnly
          value={`<a href="${url}"><img src="https://budherd.com.au${imageUrl}" alt="${title}" border="0" /></a>`}
        />
      </div>
    );
  }

  const introContent = intro ? StoryBlok.render(intro) : null;
  const postContent = body ? StoryBlok.render(body) : null;

  const pageTitle = seoTitle ? seoTitle : title;

  const citeContent = citations ? (
    <div className="citationList">
      <div className="bui-margin-top-large">
        <strong>References</strong>
      </div>
      {StoryBlok.render(citations)}
    </div>
  ) : null;

  const disclaimerContent = (
    <div className={styles.disclaimer}>
      <div>
        <strong>Disclaimer</strong>
      </div>
      {disclaimer}
    </div>
  );

  const shareImage = thumbnail
    ? `https:${thumbnail}`
    : `https:${imageUrl}`;

  const socialButtons = (
    <SocialShare
      url={url}
      description={seoDescription}
      title={title}
      image={shareImage}
    />
  );

  const coAuthor = coAuthorName
    ? {
        name: coAuthorName,
        image: coAuthorImage ? coAuthorImage.filename : null,
        profile: coAuthorSummary,
      }
    : null;

  let authorList = 'BudHerd Team';
  if (coAuthorName && !coAuthorIsTeam && !hideDefaultAuthor) {
    authorList = `${coAuthorName} & BudHerd Team`;
  } else if (coAuthorName && hideDefaultAuthor && !coAuthorIsTeam) {
    authorList = coAuthorName;
  }

  return (
    <PageLayout>
      <Row
        className={`article-wrapper article-${type}`}
        justify="space-around"
        type="flex">
        <Col
          className={styles.article}
          xs={24}
          sm={24}
          md={23}
          lg={23}
          xl={20}
          xxl={16}>
          <Content className="article-body">
            <Breadcrumb
              crumbs={[
                {
                  name: type === 'vape' ? 'Vaporizers' : 'Articles',
                  path: type === 'vape' ? '/vaporizers' : '/articles',
                },
                {
                  name: title,
                },
              ]}
            />
            <h1 className="heading-h1">{title}</h1>
            <div className={styles.postInfo}>
              <div className={styles.author}>
                {DateTime.fromISO(published_at).toFormat(
                  'LLL dd, yyyy'
                )}
                {' - '}
                by <a href="#authorlist">{authorList}</a>
              </div>
              {tagList.length ? (
                <div className="bui-flex-right">{tagList}</div>
              ) : null}
            </div>
            {postImage}
            {shareInfographic}
            {introContent && (
              <div className="intro-content">{introContent}</div>
            )}
            <Row type="flex" className="bui-flex-row-reverse">
              <Col
                className={styles.tocCol}
                span={8}
                xs={24}
                sm={24}
                md={8}>
                <div className="article-toc">
                  <Card
                    title="Table of Contents"
                    actions={[
                      <SocialShare
                        url={url}
                        description={seoDescription}
                        title={title}
                        image={shareImage}
                      />,
                    ]}>
                    <div className="toc"></div>
                  </Card>
                </div>
              </Col>
              <Col
                className={styles.contentCol}
                span={16}
                xs={24}
                sm={24}
                md={16}>
                <div className="bui-content">
                  {postContent}
                  {socialButtons}
                  <RelatedArticles tags={tag_list} slug={slug} />
                  <AuthorList
                    defaultAuthorName={defaultAuthor}
                    coAuthor={coAuthor}
                    hideDefault={hideDefaultAuthor}
                  />{' '}
                  {citeContent}
                  {disclaimerContent}
                </div>
              </Col>
            </Row>
          </Content>
        </Col>
      </Row>
      <NextSeo
        title={`${pageTitle} | BudHerd`}
        description={seoDescription}
        canonical={url}
        openGraph={{
          url,
          title: `${pageTitle} | BudHerd`,
          description: seoDescription,
          images: [
            {
              url: shareImage,
            },
          ],
        }}
      />
      <ArticleJsonLd
        url={url}
        title={pageTitle}
        images={shareImage ? [shareImage] : []}
        datePublished={created_at}
        dateModified={published_at}
        authorName={coAuthorName ? coAuthorName : 'Jason Lu'}
        publisherName="BudHerd"
        publisherLogo="https://budherd.com.au/static/favicon/favicon-196.png"
        description={seoDescription}
      />
    </PageLayout>
  );
};

export default Post;
