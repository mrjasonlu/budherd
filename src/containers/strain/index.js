import { Row, Col, Layout, Tag } from 'antd';
import { NextSeo } from 'next-seo';
import StrainSchema from './strainSchema';
import PageLayout from '~/components/layout';
import { addLineBreaks } from '~/utils/helpers';
import Breadcrumb from '~/components/breadcrumb';
import 'chartjs-plugin-datalabels';
import styles from './styles.less';
import {
  terpColors,
  createRadarChart,
  createISChart,
  createThcChart,
  createCbdChart,
  cannaLevel,
  createCardList,
} from './strainHelper';

const { Content } = Layout;
const Strain = ({ data = {} }) => {
  const {
    name,
    cannabinoids: { thc, cbd },
    category,
    description,
    effects: { positives, negatives, helps },
    flavour,
    indicaPct,
    lineage: { parents, children },
    sativaPct,
    similar,
    terps,
    stimulateScore,
    slug,
  } = data || {};

  const radarChart = createRadarChart(data);
  const barChart = createISChart(indicaPct, sativaPct);
  const thcChart = createThcChart(thc.avg);
  const cbdChart = createCbdChart(cbd.avg);

  const terpTags = terps.slice(0, 4).map((t) => (
    <Tag key={`terp-${t.name}`} color={terpColors[t.name]}>
      {t.name}
    </Tag>
  ));

  const terpList =
    terpTags && terpTags.length ? (
      <div className={styles.card}>
        <h2>Terpenes</h2>
        <div>{terpTags}</div>
      </div>
    ) : null;

  const positiveEffects = positives.map((p) => (
    <Tag key={`pos-${p}`} color="green">
      {p}
    </Tag>
  ));

  const negativeEffects = negatives.map((n) => (
    <Tag key={`neg-${n}`}>{n}</Tag>
  ));

  const effectsList =
    (positiveEffects && positiveEffects.length) ||
    (negativeEffects && negativeEffects.length) ? (
      <div className={styles.card}>
        <h2>Effects</h2>
        <div>
          {positiveEffects}
          {negativeEffects}
        </div>
      </div>
    ) : null;

  const helpEffects = helps.slice(0, 8).map((h) => (
    <Tag key={`help-${h}`} color="blue">
      {h}
    </Tag>
  ));
  const helpList =
    helpEffects && helpEffects.length ? (
      <div className={styles.card}>
        <h2>Known Usage</h2>
        <div>{helpEffects}</div>
      </div>
    ) : null;

  const flavourTags = flavour.map((f) => (
    <Tag key={`flavour-${f}`} color="magenta">
      {f}
    </Tag>
  ));
  const flavourList =
    flavourTags && flavourTags.length ? (
      <div className={styles.card}>
        <h2>Flavours</h2>
        <div>{flavourTags}</div>
      </div>
    ) : null;

  const parentList = createCardList(parents, 'Parents');
  const childrenList = createCardList(children, 'Children');
  const similarList = createCardList(similar);

  let seoDescription = `${name} is ${
    category[0] === 'I' ? 'an' : 'a'
  } ${category.toLowerCase()} strain with an average of ${
    thc.avg ? thc.avg : 'less than 1'
  } percent THC and ${
    cbd.avg ? cbd.avg : 'less than 1'
  } percent CBD. ${description}`;

  return (
    <PageLayout>
      <div className="article-wrapper">
        <Row justify="space-around" type="flex">
          <Col xs={24} sm={24} md={22} lg={20} xl={18} xxl={16}>
            <Content className="article-body">
              <Breadcrumb
                crumbs={[
                  {
                    name: 'Strains',
                    path: '/strains',
                  },
                  {
                    name: name,
                  },
                ]}
              />
              <div className={styles.header}>
                <div className={styles.info}>
                  <h1 className="heading-h1">{name}</h1>
                  <div>
                    <h2>{category}</h2>
                    {barChart}
                    <div className="bui-v-hidden">
                      <div>Indica: {indicaPct}%</div>
                      <div>Sativa: {sativaPct}%</div>
                    </div>
                    <div className={styles.cbPotencyWrapper}>
                      <div className={styles.cb}>
                        <div className={styles.cbInfo}>
                          <h2>THC</h2>
                          <div className={styles.cbPotencyText}>
                            {thc.avg ? thc.avg : '< 1'}%
                            <div>
                              <span>-</span>
                              {cannaLevel(thc.avg, 'thc')}
                            </div>
                          </div>
                        </div>
                        <div className={styles.cbPotency}>
                          {thcChart}
                        </div>
                      </div>
                      <div className={styles.cb}>
                        <div className={styles.cbInfo}>
                          <h2>CBD</h2>
                          <div className={styles.cbPotencyText}>
                            {cbd.avg ? cbd.avg : '< 1'}%
                            <div>
                              <span>-</span>
                              {cannaLevel(cbd.avg, 'cbd')}
                            </div>
                          </div>
                        </div>
                        <div className={styles.cbPotency}>
                          {cbdChart}
                        </div>
                      </div>
                    </div>
                    <div className={styles.stimulate}>
                      <div className="bui-flex">
                        <span>Relaxing</span>
                        <span className="bui-flex-right">
                          Stimulating
                        </span>
                      </div>
                      <div className={styles.stimulateScore}>
                        <span
                          className={styles.stimulateMarker}
                          style={{
                            '--percent': `${stimulateScore}%`,
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                {radarChart}
              </div>
              <div className={styles.contentWrapper}>
                <div className={styles.infoCard}>
                  {terpList}
                  {effectsList}
                  {helpList}
                  {flavourList}
                </div>
                <div className={styles.content}>
                  <div>
                    {description ? addLineBreaks(description) : null}
                  </div>

                  {(children && children.length) ||
                  (parents && parents.length) ? (
                    <div>
                      <h2>Lineage</h2>
                      <div className={styles.lineageWrapper}>
                        {parentList}
                        {childrenList}
                      </div>
                    </div>
                  ) : null}

                  {similarList && similarList.length ? (
                    <div>
                      <h2>Similar Strains</h2>
                      {similarList}
                    </div>
                  ) : null}
                </div>
              </div>
            </Content>
          </Col>
        </Row>
      </div>
      <NextSeo
        title={`${name} Strain - ${category} Cannabis Profile | BudHerd`}
        description={seoDescription}
        canonical={`https://budherd.com.au/strains/${slug}`}
        openGraph={{
          url: `https://budherd.com.au/strains/${slug}`,
          title: `${name} Strain Information - ${category} Cannabis Profile | BudHerd`,
          description: seoDescription,
        }}
      />
      <StrainSchema data={data} />
    </PageLayout>
  );
};

export default Strain;
