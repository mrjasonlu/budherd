import { Radar, HorizontalBar } from 'react-chartjs-2';
import { List, Card, Avatar } from 'antd';
import Link from 'next/link';
import { getInitials } from '~/utils/helpers';
import styles from './styles.less';

export const strainColors = {
  Indica: {
    bg: 'rgba(127, 28, 255, 0.2)',
    line: '#957dad',
    lightBg: 'rgba(127, 28, 255, 0.1)',
  },
  Sativa: {
    bg: 'rgba(255, 76, 59, 0.2)',
    line: '#ff9286',
    lightBg: 'rgba(255, 76, 59, 0.1)',
  },
  Hybrid: {
    bg: 'rgba(181, 255, 184, 0.5)',
    line: 'rgb(117, 202, 120)',
    lightBg: 'rgba(181, 255, 184, 0.2)',
  },
  'Sativa Dominant': {
    bg: 'rgba(254, 234, 158, 0.3)',
    line: 'rgb(255, 193, 78)',
    lightBg: 'rgba(254, 234, 158, 0.1)',
  },
  'Indica Dominant': {
    bg: 'rgba(74, 237, 230, 0.4)',
    line: 'rgb(20, 184, 177)',
    lightBg: 'rgba(74, 237, 230, 0.1)',
  },
};

export const thcOptions = {
  maintainAspectRatio: false,
  cutoutPercentage: 80,
  tooltips: {
    enabled: false,
  },
  legend: {
    display: false,
  },
  plugins: {
    datalabels: {
      color: '#fff',
      labels: {
        title: null,
        value: null,
      },
    },
  },
  scales: {
    xAxes: [
      {
        stacked: false,
        display: false,
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 100,
        },
      },
    ],
    yAxes: [
      {
        stacked: false,
        display: false,
      },
    ],
  },
};

export function createISChart(indicaPct, sativaPct, isSettings) {
  const {
    height = 35,
    width = 200,
    barThickness = 20,
    paddingRight = 70,
    hideDataLabel,
  } = isSettings || {};

  const speciesOptions = {
    responsive: true,
    layout: {
      padding: {
        left: 0,
        right: paddingRight,
        top: 0,
        bottom: 0,
      },
    },
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    tooltips: {
      enabled: false,
    },
    plugins: {
      datalabels: {
        color: function (context) {
          if (
            context.dataset.title === 'Sativa' &&
            context.dataset.data[0] < 25
          ) {
            return '#333';
          }
          return '#fff';
        },
        font: {
          weight: 'bold',
          size: '15',
          family: 'Nunito',
        },
        formatter: function (value, context) {
          return hideDataLabel
            ? null
            : `${context.dataset.title} ${value}%`;
        },
        align: 0,
        anchor: function (context) {
          if (context.dataset.data[0] <= 21) {
            return 'start';
          }
          return 'center';
        },
        offset: function (context) {
          if (context.dataset.data[0] > 21) {
            return -35;
          }
          if (
            context.dataset.title === 'Sativa' &&
            context.dataset.data[0] < 25
          ) {
            return -3;
          }
          return 0;
        },
        clip: false,
      },
    },
    scales: {
      xAxes: [
        {
          stacked: true,
          display: false,
          ticks: {
            beginAtZero: true,
            min: 0,
            max: 100,
          },
        },
      ],
      yAxes: [
        {
          stacked: true,
          display: false,
        },
      ],
    },
  };
  const barData = {
    datasets: [
      {
        label: 'Indica',
        title: 'Indica',
        barThickness: barThickness,
        data: [indicaPct],
        backgroundColor: '#957dad',
      },
      {
        label: 'Sativa',
        title: 'Sativa',
        barThickness: barThickness,
        data: [sativaPct],
        backgroundColor: '#ff9286',
      },
    ],
    label: 'Sativa',
  };
  return (
    <div className={styles.bar}>
      <HorizontalBar
        height={height}
        width={width}
        data={barData}
        options={speciesOptions}
      />
    </div>
  );
}

export function createRadarChart(
  { indicaPct, sativaPct, cannabinoids, stimulateScore, category },
  radarSettings
) {
  const { thc = {}, cbd = {} } = cannabinoids || {};
  const {
    height = 300,
    width = 300,
    fontSize = 15,
    pointRadius = 8,
  } = radarSettings || {};

  const radarOptions = {
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    tooltips: {
      enabled: false,
    },
    plugins: {
      datalabels: {
        color: '#fff',
        labels: {
          title: null,
          value: null,
        },
      },
    },
    scale: {
      ticks: {
        min: 0,
        max: 100,
        maxTicksLimit: 5,
        display: false,
      },
      pointLabels: {
        fontFamily: "'Nunito', sans-serif",
        fontSize: fontSize,
      },
    },
  };

  const relaxScore = 100 - stimulateScore;
  let thcScore = (thc.avg / 25) * 100;
  thcScore = thcScore > 100 ? 100 : thcScore;

  let cbdScore = (cbd.avg / 20) * 100;
  cbdScore = cbdScore > 100 ? 100 : cbdScore;
  const radarData = {
    datasets: [
      {
        data: [
          indicaPct,
          thcScore,
          stimulateScore,
          sativaPct,
          cbdScore,
          relaxScore,
        ],
        backgroundColor: category
          ? strainColors[category].bg
          : '#eee',
        borderColor: category ? strainColors[category].line : '#eee',
        pointBackgroundColor: category
          ? strainColors[category].line
          : '#eee',
        pointRadius: pointRadius,
        pointBorderColor: '#fff',
        pointHoverRadius: pointRadius,
        lineTension: 0.2,
      },
    ],
    labels: [
      'Indica',
      'THC',
      'Stimulating',
      'Sativa',
      'CBD',
      'Relaxing',
    ],
  };

  return (
    <div className={styles.radar}>
      <Radar
        height={height}
        width={width}
        data={radarData}
        options={radarOptions}
      />
    </div>
  );
}

export function createThcChart(avg) {
  const thcData = (canvas) => {
    const ctx = canvas.getContext('2d');
    const gradient = ctx.createLinearGradient(0, 0, 150, 0);
    gradient.addColorStop(0, 'rgba(255, 76, 59,0.5)');
    gradient.addColorStop(1, 'rgba(149, 125, 173,1)');

    let thcScore = (avg / 25) * 100;
    thcScore = thcScore > 100 ? 100 : thcScore;
    return {
      datasets: [
        {
          barThickness: 8,
          label: 'My First dataset',
          data: [thcScore],
          backgroundColor: [gradient, '#eeeeee'],
          hoverBorderColor: 'rgba(0,0,0,0)',
        },
      ],
    };
  };

  return (
    <HorizontalBar
      data={thcData}
      height={40}
      width={50}
      options={thcOptions}
    />
  );
}

export function createCbdChart(avg) {
  const thcData = (canvas) => {
    const ctx = canvas.getContext('2d');
    const gradient = ctx.createLinearGradient(0, 0, 150, 0);
    gradient.addColorStop(0, 'rgba(255, 76, 59,0.5)');
    gradient.addColorStop(1, 'rgba(149, 125, 173,1)');

    let thcScore = (avg / 20) * 100;
    thcScore = thcScore > 100 ? 100 : thcScore;
    return {
      datasets: [
        {
          barThickness: 8,
          label: 'My First dataset',
          data: [thcScore],
          backgroundColor: [gradient, '#eeeeee'],
          hoverBorderColor: 'rgba(0,0,0,0)',
        },
      ],
    };
  };

  return (
    <HorizontalBar
      data={thcData}
      height={40}
      width={50}
      options={thcOptions}
    />
  );
}

export function cannaLevel(pct, type) {
  if (type === 'cbd') {
    if (!pct) {
      return 'Very Low';
    } else if (pct > 0 && pct <= 2) {
      return 'Low';
    } else if (pct > 2 && pct <= 10) {
      return 'Average';
    } else if (pct > 10 && pct <= 15) {
      return 'High';
    } else if (pct > 15) {
      return 'Very High';
    }
  }

  if (type === 'thc') {
    if (!pct) {
      return 'NA';
    } else if (pct > 0 && pct <= 5) {
      return 'Low';
    } else if (pct > 5 && pct <= 13) {
      return 'Average';
    } else if (pct > 13 && pct <= 18) {
      return 'High';
    } else if (pct > 18) {
      return 'Very High';
    }
  }
}

export function createCardList(rawData, title) {
  const listData = rawData.map((d) => ({
    name: d.name,
    slug: d.slug,
    category: d.category,
  }));

  if (!(listData && listData.length)) {
    return null;
  }

  return (
    <div className={styles.cardList}>
      {title ? <h3>{title}</h3> : null}
      <List
        grid={{
          gutter: 16,
          column: 1,
          md: 2,
          sm: 1,
        }}
        dataSource={listData}
        renderItem={(item) => {
          if (item.slug) {
            return (
              <List.Item>
                <Link
                  href="/strains/[slug]"
                  as={`/strains/${item.slug}`}>
                  <a>
                    <Card hoverable size="small">
                      <div className="bui-flex bui-align-center">
                        <Avatar
                          style={{
                            backgroundColor:
                              strainColors[item.category].line,
                            verticalAlign: 'middle',
                          }}
                          className={styles.cardAvatar}>
                          {getInitials(item.category)}
                        </Avatar>
                        <div className={styles.cardName}>
                          {item.name}
                        </div>
                      </div>
                    </Card>
                  </a>
                </Link>
              </List.Item>
            );
          } else {
            return (
              <List.Item>
                <Card size="small">
                  <div className="bui-flex bui-align-center">
                    <Avatar
                      style={{
                        backgroundColor: '#888888',
                        verticalAlign: 'middle',
                      }}
                      className={styles.cardAvatar}>
                      {getInitials(item.name)}
                    </Avatar>
                    <div className={styles.cardName}>{item.name}</div>
                  </div>
                </Card>
              </List.Item>
            );
          }
        }}
      />
    </div>
  );
}

export const terpColors = {
  Myrcene: 'cyan',
  Pinene: 'green',
  Caryophyllene: 'red',
  Limonene: 'lime',
  Humulene: 'blue',
  Linalool: 'geekblue',
  Terpinolene: 'purple',
  Ocimene: 'orange',
};
