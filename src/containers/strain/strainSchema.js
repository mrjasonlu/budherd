import Head from 'next/head';

function makeStrainSchema(data) {
  const { name, description, category } = data;
  return {
    '@context': 'https://schema.org/',
    '@type': 'Product',
    name,
    category,
    additionalType: 'http://www.productontology.org/id/Cannabis',
    description,
    aggregateRating: {
      '@type': 'AggregateRating',
      ratingValue: 0,
      reviewCount: 0,
    },
  };
}

const StrainSchema = ({ data }) => {
  return (
    <Head>
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(makeStrainSchema(data)),
        }}
      />
    </Head>
  );
};
export default StrainSchema;
