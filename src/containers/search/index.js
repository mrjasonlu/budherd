import { useState, useEffect } from 'react';
import { Row, Col, Layout, Tabs } from 'antd';
import { ArrowRightOutlined } from '@ant-design/icons';
import { NextSeo } from 'next-seo';
import PageLayout from '~/components/layout';
import ArticleList from '~/components/articleList';
import StrainGrid from '~/components/strainGrid';
import StrainList from '~/components/strainListing';
import styles from './styles.less';
const TabPane = Tabs.TabPane;

const { Content } = Layout;
const Search = ({ strains, posts, context }) => {
  const {
    query: { q: searchQuery },
  } = context;
  const [activeTab, setActiveTab] = useState('1');
  const [strainListKey, setstrainListKey] = useState(
    new Date().getTime()
  );

  useEffect(() => {
    setActiveTab('1');
    setstrainListKey(new Date().getTime());
  }, [context.query.q]);
  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <Col xs={24} sm={24} md={22} lg={20} xl={18} xxl={16}>
          <Content className="article-body">
            <h1 className="heading-h1">Search results</h1>
            {searchQuery ? (
              <p>
                Showing results for "
                <span className="bui-color-green bui-padding-horizontal-xsmall">
                  {searchQuery}
                </span>
                "
              </p>
            ) : null}
            <Tabs
              defaultActiveKey="1"
              activeKey={activeTab}
              onTabClick={(key) => setActiveTab(key)}
              animated={false}
              className={styles.results}>
              <TabPane tab="All" key="1">
                {!(strains.data && strains.data.length) &&
                !(posts && posts.length) ? (
                  <p>No matching results</p>
                ) : null}
                <div className="article-feed">
                  {posts.length ? (
                    <h2 className="heading-category">
                      Article results
                    </h2>
                  ) : null}
                  <ArticleList posts={posts} count={3} />{' '}
                  {posts.length ? (
                    <div className="article-feed-cta">
                      <a onClick={() => setActiveTab('3')}>
                        All articles
                        <ArrowRightOutlined />
                      </a>
                    </div>
                  ) : null}
                </div>
                <div className="article-feed">
                  {strains.data.length ? (
                    <h2 className="heading-category">
                      Strain results
                    </h2>
                  ) : null}
                  <StrainGrid
                    strains={strains.data}
                    count={3}
                    grid={{
                      span: 8,
                      xs: 24,
                      sm: 24,
                      md: 12,
                      lg: 8,
                    }}
                  />{' '}
                  {strains.data.length ? (
                    <div className="article-feed-cta">
                      <a onClick={() => setActiveTab('2')}>
                        All strains
                        <ArrowRightOutlined />
                      </a>
                    </div>
                  ) : null}
                </div>
              </TabPane>
              <TabPane tab="Strains" key="2">
                {strains.data && strains.data.length ? null : (
                  <p>No matching strain results.</p>
                )}
                <StrainList
                  key={strainListKey}
                  strains={strains.data}
                  context={context}
                  count="24"
                  hideSort
                  total={strains.total}
                />
              </TabPane>
              <TabPane tab="Articles" key="3">
                {posts && posts.length ? null : (
                  <p>No matching article results.</p>
                )}
                <ArticleList posts={posts} count={posts.length} />
              </TabPane>
            </Tabs>
          </Content>
        </Col>
      </Row>
      <NextSeo
        title={`Search Results for '${searchQuery}' | BudHerd`}
        description={`Displaying BudHerd search results for term '${searchQuery}'`}
        canonical={`https://budherd.com.au/search?q=${searchQuery}`}
        openGraph={{
          url: `https://budherd.com.au/search?q=${searchQuery}`,
          title: `Search results for '${searchQuery}' | BudHerd`,
          description: `Displaying BudHerd website search results for term '${searchQuery}'`,
        }}
      />
    </PageLayout>
  );
};

export default Search;
