import { Row, Col, Layout } from 'antd';
import PageLayout from '~/components/layout';
import ArticleList from '~/components/articleList';
import Breadcrumb from '~/components/breadcrumb';
import { NextSeo } from 'next-seo';
import { ArrowRightOutlined } from '@ant-design/icons';
import Link from 'next/link';
import styles from './styles.less';
const imageMain = require('./vape-bg.jpg?resize&sizes[]=300&sizes[]=600&sizes[]=800&sizes[]=1000');

const Vaporizers = ({ posts, vapes }) => {
  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <Breadcrumb
          crumbs={[
            {
              name: 'Vaporizers',
              path: '/vaporizers',
            },
          ]}
        />
        <Col span={24}>
          <div className="pageBanner landingBanner">
            <div className="bannerImage">
              <img
                alt="hand holding Mighty Vaporizer"
                srcSet={imageMain.srcSet}
                src={imageMain.src}
                sizes="(min-width: 767px) 50vw, 100vw"
              />
            </div>
            <div className="bannerWrapper">
              <div className="bannerText">
                <h1>Dry Herb Vaporizers 101</h1>
                <p>
                  Ultimate guide on dry herb vaporizers for Aussies,
                  including articles to help you navigate your first
                  purchase and how they affect the way you consume
                  cannabis.
                </p>
              </div>
            </div>
          </div>
        </Col>

        <Col
          xs={24}
          sm={24}
          md={22}
          lg={20}
          xl={18}
          xxl={16}
          className="bui-margin-top-xxlarge">
          {/* <Row justify="center">
            <Col span={24} md={24} lg={12} justify="center">
              <Link href="#">
                <a
                  href="#"
                  className={`${styles.portableBg} ${styles.vapeCol} bui-flex bui-align-center bui-justify-center bui-flex-col`}>
                  <h2 className="bui-text-center">
                    Best Portable Vaporizers 2022
                  </h2>
                  <div>
                    <img src="https://a.storyblok.com/f/81752/393x269/b53aede06f/best-portable-vape.png/m/" />
                  </div>
                </a>
              </Link>
            </Col>
            <Col span={24} md={24} lg={12} justify="center">
              <Link href="#">
                <a
                  href="#"
                  className={`${styles.desktopBg} ${styles.vapeCol} bui-flex bui-align-center bui-justify-center bui-flex-col`}>
                  <h2 className="bui-text-center">
                    Best Desktop Vaporizers 2022
                  </h2>
                  <div>
                    <img src="https://a.storyblok.com/f/81752/393x269/35271c05c5/best-desktop-vape.png/m/" />
                  </div>
                </a>
              </Link>
            </Col>
          </Row> */}

          <div className="article-feed bui-margin-top-xxlarge">
            <Row justify="center">
              <Col
                span={24}
                xs={22}
                md={24}
                xxl={24}
                justify="center">
                <h2 className="heading-category">Vaporizers 101</h2>
                <ArticleList
                  posts={posts}
                  count={9}
                  getPath={(article) =>
                    article && article.content.component === 'vape'
                      ? 'vaporizers'
                      : 'articles'
                  }
                />
                {/* <div className="article-feed-cta">
                  <Link href={`/articles`}>
                    <a href="/articles">
                      More Articles
                      <ArrowRightOutlined />
                    </a>
                  </Link>
                </div> */}
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
      <NextSeo
        title="Dry Herb Vaporizers | BudHerd"
        description="Dry herb vaporizer resource for Australians. Learn about the difference between convection and coduction "
        canonical="https://budherd.com.au/vaporizer"
        openGraph={{
          url: 'https://budherd.com.au/vaporizer',
          title:
            'Cannabis Education and Australian Medical Marijuana Stories | BudHerd',
          description:
            'Australian resource to learn about cannabis, CBD oils, medical marijuana applications and legislation.',
          site_name: 'BudHerd',
        }}
      />
    </PageLayout>
  );
};

export default Vaporizers;
