import { Layout, Col, Row } from 'antd';
import Link from 'next/link';
import { Defer } from 'react-progressive-loader';
import { NextSeo } from 'next-seo';
import PageLayout from '~/components/layout';
import ArticleList from '~/components/articleList';
import StrainGrid from '~/components/strainGrid';
import { ArrowRightOutlined } from '@ant-design/icons';
const { Content } = Layout;
import styles from './styles.less';
const imageMain = require('./cannabis-smoke.jpg?resize&sizes[]=300&sizes[]=600&sizes[]=800&sizes[]=1000');

const Home = ({ posts, strains }) => {
  return (
    <PageLayout>
      <Row
        className={styles.homeWrapper}
        justify="space-around"
        type="flex">
        <div className="pageBanner">
          <div className="bannerImage">
            <img
              alt="person smoking cannabis"
              srcSet={imageMain.srcSet}
              src={imageMain.src}
              sizes="(min-width: 767px) 50vw, 100vw"
            />
          </div>
          <div className="bannerWrapper">
            <div className="bannerText">
              <h1>Find, Explore and Learn all things cannabis.</h1>
              <p>
                We at BudHerd are setting the record straight on the
                medicinal and recreational use of cannabis for
                Australians.
              </p>
            </div>
          </div>
        </div>
        <Col span={18} xs={23} md={20} lg={18}>
          <Content>
            <div className={styles.homeIntro}>
              <p>
                We are on a mission to educate Australia on the facts
                and benefits of cannabis.
                <br />
                From the many medicinal effects of CBD oils to
                exploring the THC potency and terpenes of different
                cannabis strains - we've got you covered.
              </p>
            </div>
            <div className="article-feed">
              <Row justify="center">
                <Col
                  span={24}
                  xs={22}
                  md={24}
                  xxl={20}
                  justify="center">
                  <h2 className="heading-category">
                    Learn about cannabis
                  </h2>
                  <ArticleList posts={posts} count={9} />
                  <div className="article-feed-cta">
                    <Link href={`/articles`}>
                      <a href="/articles">
                        More Articles
                        <ArrowRightOutlined />
                      </a>
                    </Link>
                  </div>
                </Col>
              </Row>
            </div>
            <div className="article-feed">
              <Row justify="center">
                <Col
                  span={24}
                  xs={22}
                  md={24}
                  xxl={20}
                  justify="center">
                  <h2 className="heading-category ">
                    Explore Strains
                  </h2>
                  <Defer
                    render={() => (
                      <StrainGrid
                        strains={strains.data}
                        count={
                          strains && strains.data
                            ? strains.data.length
                            : 0
                        }
                        grid={{
                          span: 8,
                          xs: 24,
                          sm: 24,
                          md: 12,
                          lg: 8,
                        }}
                      />
                    )}
                    renderPlaceholder={() => null}
                    loadOnScreen
                  />

                  <div className="article-feed-cta">
                    <Link href={`/strains`}>
                      <a href="/strains">
                        More Strains
                        <ArrowRightOutlined />
                      </a>
                    </Link>
                  </div>
                </Col>
              </Row>
            </div>
          </Content>
        </Col>
      </Row>
      <NextSeo
        title="Explore Cannabis Strains, CBD Oils, and Australian Medical Marijuana Stories | BudHerd"
        description="Explore and learn about cannabis strains, CBD oils and the current state of medical marijuana in Australia."
        canonical="https://budherd.com.au/"
        openGraph={{
          url: 'https://budherd.com.au/',
          title: 'Expolre all things Cannabis Australia | BudHerd',
          description:
            'Explore and learn about cannabis strains, CBD oils and the current state of medical marijuana in Australia.',
          site_name: 'BudHerd',
        }}
      />
    </PageLayout>
  );
};

export default Home;
