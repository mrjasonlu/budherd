import { Row, Col, Layout } from 'antd';
import { NextSeo } from 'next-seo';
import PageLayout from '~/components/layout';
import StrainList from '~/components/strainListing';

const { Content } = Layout;
const Strains = ({ data = {}, context, total }) => {
  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <Col xs={24} sm={24} md={22} lg={20} xl={18} xxl={16}>
          <Content className="article-body">
            <h1 className="heading-h1">Strain Explorer</h1>
            <StrainList
              strains={data}
              context={context}
              count="24"
              total={total}
            />
          </Content>
        </Col>
      </Row>
      <NextSeo
        title="Explore cannabis and marijuana strains | BudHerd"
        description="Browse our list of sativa, indica and hybrid cannabis strains with cannabinoid and terpene profiles."
        canonical="https://budherd.com.au/strains"
        openGraph={{
          url: 'https://budherd.com.au/strains',
          title: 'Explore cannabis and marijuana strains | BudHerd',
          description:
            'Browse our list of sativa, indica and hybrid cannabis strains with cannabinoid and terpene profiles.',
        }}
      />
    </PageLayout>
  );
};

export default Strains;
