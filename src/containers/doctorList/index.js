import { Row, Col } from 'antd';
import PageLayout from '~/components/layout';
import { NextSeo } from 'next-seo';
import { layoutSizes } from '~/config';
import Breadcrumb from '~/components/breadcrumb';
import BusinessList from '~/components/businessList';
import StateForm from '~/components/stateSelector';
import Link from 'next/link';
import { disclaimer } from '~/config/lexicon';
import styles from './styles.less';
const imageMain = require('~/assets/images/doctor.jpg?resize&sizes[]=300&sizes[]=600&sizes[]=800&sizes[]=10' +
  '00');

const filters = [
  {
    labels: {
      doctor: 'Medical Practioners',
      clinic: 'Clinics',
    },
    name: 'category',
  },
];

const stateNames = {
  vic: 'Victoria',
  nsw: 'New South Wales',
  nt: 'Northern Territory',
  wa: 'Western Australia',
  sa: 'South Australia',
  tas: 'Tasmania',
  qld: 'Queensland',
  nt: 'Northern Territory',
  act: 'Australian Capital Territory',
};

const DoctorList = ({ list, state, context }) => {
  const emptyContent = (
    <div>
      No matching results.
      <br />
      <Link
        href="/nearme/doctors/[state]"
        as={`/nearme/doctors/${state}`}>
        <a>Remove the search filters</a>
      </Link>{' '}
      to see a full list of medical practitioners and clinics.
    </div>
  );

  const footerContent = (
    <div className="bui-margin-top-large">
      Please feel free to{' '}
      <Link href={`/contact`}>
        <a>contact us</a>
      </Link>{' '}
      if you know any medical practitioners who should be added to the
      list or if you wish to report an issue with an existing contact.
    </div>
  );

  const headerContent = (
    <div className={styles.stateForm}>
      <h3>State</h3>
      <StateForm
        path="/nearme/doctors/"
        selected={state}
        submitOnSelect
      />
    </div>
  );

  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <Col {...layoutSizes}>
          <Breadcrumb
            crumbs={[
              {
                name: 'Doctors',
                path: '/nearme/doctors',
              },
              {
                name: state.toUpperCase(),
                path: `/nearme/doctors/${state}`,
              },
            ]}
          />
        </Col>
        <Col {...layoutSizes}>
          <BusinessList
            title={`Find experienced medicinal cannabis doctors - ${stateNames[state]}`}
            filterHeader={headerContent}
            path={{
              query: context.query,
              full: `/nearme/doctors/${state}`,
            }}
            categorise={{
              clinic: 'Clinics',
              doctor: 'Medical Practitioners',
              telehealth: 'Interstate Telehealth',
            }}
            subCategory={(type) =>
              type === 'TELE' ? 'telehealth' : null
            }
            profile={({ category }) => category === 'doctor'}
            state={state}
            categorySort={(a, b) => {
              if (a === 'doctor') {
                return 1;
              }
              if (a === 'telehealth' && b === 'doctor') {
                return -1;
              }
              return 0;
            }}
            filters={filters}
            list={list}
            emptyContent={emptyContent}
            footerContent={footerContent}
          />

          <div className={styles.disclaimer}>
            <div>
              <strong>Disclaimer</strong>
            </div>
            This listing is provided by BudHerd for potential patients
            requiring advice on medicinal cannabis. BudHerd does not
            guarantee or imply the doctors in this list will endorse
            or prescribe medicinal cannabis products. {disclaimer}
          </div>
        </Col>
      </Row>
      <NextSeo
        title={`Find Medicinal Cannabis Doctors in ${stateNames[state]} | BudHerd`}
        description={`Find doctors and clinics experienced with prescribing medicinal cannabis products in ${stateNames[state]}.`}
        canonical={`https://budherd.com.au/nearme/doctors/${state}`}
        openGraph={{
          images: [
            {
              url: imageMain.src,
              alt: 'Find medicinal cannabis docotrs Australia',
            },
          ],
          url: `https://budherd.com.au/nearme/doctors/${state}`,
          title: `Find Medicinal Cannabis Doctors in ${stateNames[state]} | BudHerd`,
          description: `Find doctors and clinics experienced with prescribing medicinal cannabis products in ${stateNames[state]}.`,
        }}
      />
    </PageLayout>
  );
};

export default DoctorList;
