import { Row, Col, Layout } from 'antd';
import PageLayout from '~/components/layout';
import ArticleList from '~/components/articleList';
import Breadcrumb from '~/components/breadcrumb';
import { NextSeo } from 'next-seo';
const imageMain = require('./cannabis-education.jpg?resize&sizes[]=300&sizes[]=600&sizes[]=800&sizes[]=1000');

const { Content } = Layout;
const Articles = ({ posts }) => {
  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <Breadcrumb
          crumbs={[
            {
              name: 'Cannabis Education',
              path: '/articles',
            },
          ]}
        />
        <Col span={24}>
          <div className="pageBanner landingBanner">
            <div className="bannerImage">
              <img
                alt="hand holding marijuana leaf"
                srcSet={imageMain.srcSet}
                src={imageMain.src}
                sizes="(min-width: 767px) 50vw, 100vw"
              />
            </div>
            <div className="bannerWrapper">
              <div className="bannerText">
                <h1>Cannabis Education</h1>
                <p>
                  Start exploring different cannabis topics with our
                  curated facts and information for Australians.
                </p>
              </div>
            </div>
          </div>
        </Col>
        <Col xs={24} sm={24} md={22} lg={20} xl={18} xxl={16}>
          <Content className="article-body bui-padding-top-xxxlarge">
            <ArticleList posts={posts} />
          </Content>
        </Col>
      </Row>
      <NextSeo
        title="Cannabis Education and Australian Medicinal Marijuana Stories | BudHerd"
        description="Australian resource to learn about cannabis, CBD oils, medical marijuana applications and legislation."
        canonical="https://budherd.com.au/articles"
        openGraph={{
          url: 'https://budherd.com.au/articles',
          title:
            'Cannabis Education and Australian Medical Marijuana Stories | BudHerd',
          description:
            'Australian resource to learn about cannabis, CBD oils, medical marijuana applications and legislation.',
          site_name: 'BudHerd',
        }}
      />
    </PageLayout>
  );
};

export default Articles;
