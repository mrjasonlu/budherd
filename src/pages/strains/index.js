import { fetchStrains } from '~/resources';
import StrainContainer from '~/containers/strains';

const Strains = (data = {}) => {
  const { strains, context, total } = data;
  return (
    <StrainContainer data={strains} context={context} total={total} />
  );
};

Strains.getInitialProps = async function (context) {
  const { query, pathname } = context;
  const { _page = 1, _sort, _order } = query || {};
  const res = await fetchStrains({ _page, _sort, _order });
  const { data, total } = res;
  return { strains: data, context: { query, pathname }, total };
};

export default Strains;
