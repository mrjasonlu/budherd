import { fetchStrain } from '~/resources';
import StrainProfile from '~/containers/strain';

const Strain = (data = {}) => {
  return <StrainProfile data={data.strain} />;
};

Strain.getInitialProps = async function (context) {
  const { slug } = context.query;
  const strain = await fetchStrain(slug);
  return { strain };
};

export default Strain;
