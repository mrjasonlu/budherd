import { searchStrains, searchPosts } from '~/resources';
import SearchContainer from '~/containers/search';

const Strains = (data = {}) => {
  const { strains, posts, context } = data;
  return (
    <SearchContainer
      strains={strains}
      posts={posts}
      context={context}
    />
  );
};

Strains.getInitialProps = async function (context) {
  const { query = {}, pathname } = context;
  const { _page = 1 } = query;
  const { q } = query;

  const strains = await searchStrains(q, _page);
  const posts = await searchPosts(q);
  return { strains, posts, context: { query, pathname } };
};

export default Strains;
