import Document, { Head, Main, NextScript } from 'next/document';
import { makeOrgSchema, makeSiteSchema } from '~/utils/siteSchema';
import StoryblokService from '~/utils/storyBlok';
import { GA_TRACKING_ID } from '~/utils/gtag';

export default class MyDocument extends Document {
  render() {
    return (
      <html lang="en">
        <Head>
          <style
            dangerouslySetInnerHTML={{
              __html: `/* cyrillic-ext */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: local('Merriweather Regular'), local('Merriweather-Regular'), url(https://fonts.gstatic.com/s/merriweather/v22/u-440qyriQwlOrhSvowK_l5-cSZMdeX3rsHo.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
              }
              /* cyrillic */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: local('Merriweather Regular'), local('Merriweather-Regular'), url(https://fonts.gstatic.com/s/merriweather/v22/u-440qyriQwlOrhSvowK_l5-eCZMdeX3rsHo.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
              }
              /* vietnamese */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: local('Merriweather Regular'), local('Merriweather-Regular'), url(https://fonts.gstatic.com/s/merriweather/v22/u-440qyriQwlOrhSvowK_l5-cyZMdeX3rsHo.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
              }
              /* latin-ext */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: local('Merriweather Regular'), local('Merriweather-Regular'), url(https://fonts.gstatic.com/s/merriweather/v22/u-440qyriQwlOrhSvowK_l5-ciZMdeX3rsHo.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
              }
              /* latin */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 400;
                font-display: swap;
                src: local('Merriweather Regular'), local('Merriweather-Regular'), url(https://fonts.gstatic.com/s/merriweather/v22/u-440qyriQwlOrhSvowK_l5-fCZMdeX3rg.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
              }
              /* cyrillic-ext */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Merriweather Bold'), local('Merriweather-Bold'), url(https://fonts.gstatic.com/s/merriweather/v22/u-4n0qyriQwlOrhSvowK_l52xwNZVcf6hPvhPUWH.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
              }
              /* cyrillic */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Merriweather Bold'), local('Merriweather-Bold'), url(https://fonts.gstatic.com/s/merriweather/v22/u-4n0qyriQwlOrhSvowK_l52xwNZXMf6hPvhPUWH.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
              }
              /* vietnamese */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Merriweather Bold'), local('Merriweather-Bold'), url(https://fonts.gstatic.com/s/merriweather/v22/u-4n0qyriQwlOrhSvowK_l52xwNZV8f6hPvhPUWH.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
              }
              /* latin-ext */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Merriweather Bold'), local('Merriweather-Bold'), url(https://fonts.gstatic.com/s/merriweather/v22/u-4n0qyriQwlOrhSvowK_l52xwNZVsf6hPvhPUWH.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
              }
              /* latin */
              @font-face {
                font-family: 'Merriweather';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Merriweather Bold'), local('Merriweather-Bold'), url(https://fonts.gstatic.com/s/merriweather/v22/u-4n0qyriQwlOrhSvowK_l52xwNZWMf6hPvhPQ.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
              }
              /* cyrillic-ext */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 300;
                font-display: swap;
                src: local('Nunito Light'), local('Nunito-Light'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAnsSUbOvIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
              }
              /* cyrillic */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 300;
                font-display: swap;
                src: local('Nunito Light'), local('Nunito-Light'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAnsSUZevIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
              }
              /* vietnamese */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 300;
                font-display: swap;
                src: local('Nunito Light'), local('Nunito-Light'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAnsSUbuvIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
              }
              /* latin-ext */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 300;
                font-display: swap;
                src: local('Nunito Light'), local('Nunito-Light'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAnsSUb-vIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
              }
              /* latin */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 300;
                font-display: swap;
                src: local('Nunito Light'), local('Nunito-Light'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAnsSUYevIWzgPDA.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
              }
              /* cyrillic-ext */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 600;
                font-display: swap;
                src: local('Nunito SemiBold'), local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofA6sKUbOvIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
              }
              /* cyrillic */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 600;
                font-display: swap;
                src: local('Nunito SemiBold'), local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofA6sKUZevIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
              }
              /* vietnamese */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 600;
                font-display: swap;
                src: local('Nunito SemiBold'), local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofA6sKUbuvIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
              }
              /* latin-ext */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 600;
                font-display: swap;
                src: local('Nunito SemiBold'), local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofA6sKUb-vIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
              }
              /* latin */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 600;
                font-display: swap;
                src: local('Nunito SemiBold'), local('Nunito-SemiBold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofA6sKUYevIWzgPDA.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
              }
              /* cyrillic-ext */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Nunito Bold'), local('Nunito-Bold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAjsOUbOvIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
              }
              /* cyrillic */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Nunito Bold'), local('Nunito-Bold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAjsOUZevIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
              }
              /* vietnamese */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Nunito Bold'), local('Nunito-Bold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAjsOUbuvIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
              }
              /* latin-ext */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Nunito Bold'), local('Nunito-Bold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAjsOUb-vIWzgPDEtj.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
              }
              /* latin */
              @font-face {
                font-family: 'Nunito';
                font-style: normal;
                font-weight: 700;
                font-display: swap;
                src: local('Nunito Bold'), local('Nunito-Bold'), url(https://fonts.gstatic.com/s/nunito/v14/XRXW3I6Li01BKofAjsOUYevIWzgPDA.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
              }              
              #nprogress{pointer-events:none}#nprogress .bar{background:#29d;position:fixed;z-index:1031;top:0;left:0;width:100%;height:2px}#nprogress .peg{display:block;position:absolute;right:0;width:100px;height:100%;box-shadow:0 0 10px #29d,0 0 5px #29d;opacity:1;-webkit-transform:rotate(3deg) translate(0,-4px);-ms-transform:rotate(3deg) translate(0,-4px);transform:rotate(3deg) translate(0,-4px)}#nprogress .spinner{display:block;position:fixed;z-index:1031;top:15px;right:15px}#nprogress .spinner-icon{width:18px;height:18px;box-sizing:border-box;border:solid 2px transparent;border-top-color:#29d;border-left-color:#29d;border-radius:50%;-webkit-animation:nprogress-spinner .4s linear infinite;animation:nprogress-spinner .4s linear infinite}.nprogress-custom-parent{overflow:hidden;position:relative}.nprogress-custom-parent #nprogress .bar,.nprogress-custom-parent #nprogress .spinner{position:absolute}@-webkit-keyframes nprogress-spinner{0%{-webkit-transform:rotate(0)}100%{-webkit-transform:rotate(360deg)}}@keyframes nprogress-spinner{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}
              .toc {
                overflow-y: auto;
                font-size: 1em;
                line-height: 2em;
                margin-left: 0;
              }
              .toc > .toc-list {
                overflow: hidden;
                position: relative;
              }
              .toc > .toc-list li {
                list-style: none;
              }
              
              .toc > .toc-list li a {
                padding: 10px 0;
              }
              
              .toc-list {
                margin: 0;
                padding-left: 10px;
              }
              a.toc-link {
                color: currentColor;
                height: 100%;
              }
              .is-collapsible {
                max-height: 1000px;
                overflow: hidden;
                transition: all 300ms ease-in-out;
              }
              .is-collapsed {
                max-height: 0;
              }
              
              .is-active-link {
                font-weight: 700;
              }
              .toc-link::before {
                background-color: #eee;
                content: ' ';
                display: inline-block;
                height: inherit;
                left: 0;
                margin-top: -1px;
                position: absolute;
                width: 2px;
              }
              .is-active-link::before {
                background-color: #54bc4b;
              }
              
              `,
            }}
          />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/static/favicon/favicon-180.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/static/favicon/favicon-32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/static/favicon/favicon-16.png"
          />
          <link rel="manifest" href="/static/favicon/manifest.json" />
          <link
            rel="mask-icon"
            href="/static/favicon/favicon-pinned.svg"
            color="#2d8989"
          />
          <link
            rel="shortcut icon"
            href="/static/favicon/favicon.ico"
          />
          <meta name="apple-mobile-web-app-title" content="BudHerd" />
          <meta name="application-name" content="BudHerd" />
          <meta name="theme-color" content="#2d8989" />
          <meta property="og:site_name" content="BudHerd" />
          <meta property="og:type" content="website" />
          <meta
            name="google-site-verification"
            content="UWu02DOEXxE_noYoiERnD7U_hYDqqlWmPrC_qNR4Msg"
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `var StoryblokCacheVersion = '${StoryblokService.getCacheVersion()}';`,
            }}></script>
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: JSON.stringify(makeOrgSchema()),
            }}
          />
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: JSON.stringify(makeSiteSchema()),
            }}
          />{' '}
          {process.env.NODE_ENV !== 'development' ? (
            <script
              async
              src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
            />
          ) : null}
          {process.env.NODE_ENV !== 'development' ? (
            <script
              dangerouslySetInnerHTML={{
                __html: ` window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', '${GA_TRACKING_ID}', { page_path: window.location.pathname, }); `,
              }}
            />
          ) : null}
        </Head>
        <body className="bui">
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
