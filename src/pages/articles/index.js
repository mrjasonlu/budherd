import { fetchPosts } from '~/resources';
import ArticlesContainer from '~/containers/articles';

const Articles = (data = {}) => {
  return <ArticlesContainer posts={data} />;
};

Articles.getInitialProps = async function () {
  return await fetchPosts({ starts_with: 'posts/' });
};

export default Articles;
