import { Row, Col, Layout } from 'antd';
import PageLayout from '~/components/layout';
import { NextSeo } from 'next-seo';
import Breadcrumb from '~/components/breadcrumb';
const imageMain = require('~/assets/images/cannabis-sunset.jpg?resize&sizes[]=300&sizes[]=600&sizes[]=800&sizes[]=1000');

const { Content } = Layout;
export default ({ posts }) => {
  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <div className="pageBanner landingBanner">
          <div className="bannerImage">
            <img
              alt="Cannabis field in the sunset"
              srcSet={imageMain.srcSet}
              src={imageMain.src}
              sizes="(min-width: 767px) 50vw, 100vw"
            />
          </div>
          <div className="bannerWrapper">
            <div className="bannerText">
              <h1>About BudHerd</h1>
              <p>
                We are passionate about cannabis, and are committed to
                educating Australians on the facts and science of this
                underrated and often discredited plant.
              </p>
            </div>
          </div>
        </div>
        <Col xs={24} sm={24} md={19} lg={16} xl={13} xxl={12}>
          <Content className="article-body">
            <div className="bui-content">
              <p>
                Our mission is to cultivate, debunk and spread
                awareness of the health and medicinal benefits of
                cannabis, as we firmly believe all Aussies should have
                enough information and tools to make their own
                judgements.
              </p>
              <p>
                Since the new Australian Capital Territory cultivation
                and possession laws come into effect in ACT in January
                2020, we've been busy curating content and driving the
                Australian cannabis revolution through the medium we
                are best at.
              </p>
              <p>
                Australia is still falling behind the curve from the
                rest of the world in terms of cultural acceptance and
                education of cannabis, even though medicinal use had
                been legalised across most of the country for several
                years. Majority of patients suffering from a variety
                of conditions such as epilepsy, anxiety and chronic
                pain are still unaware of the therapeutic effects of
                cannabis that could assist in reducing or ease the
                symptoms.
              </p>
              <p>
                Our website aims to provide our community with
                detailed cannabis strain analysis data along with
                useful articles outlining the science and facts behind
                cannabis, and how it could benefit the health and
                wellbeing of everyday Australians.
              </p>
            </div>
          </Content>
        </Col>
      </Row>
      <NextSeo
        title="About | BudHerd"
        description="We are passionate about cannabis, and are committed to educating Australians on the facts and science of this underrated and often discredited plant."
        canonical="https://budherd.com.au/about"
        openGraph={{
          url: 'https://budherd.com.au/about',
          title: 'About | BudHerd',
          description:
            'We are passionate about cannabis, and are committed to educating Australians on the facts and science of this underrated and often discredited plant.',
        }}
      />
    </PageLayout>
  );
};
