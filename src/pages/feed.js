import { DateTime } from 'luxon';
import { fetchPosts } from '~/resources';

const articleXml = (articles) => {
  let postsXml = '';
  articles.map((article) => {
    const postDate = Date.parse(article.first_published_at);
    let tagList = '';
    article.tag_list.forEach(
      (tag) => (tagList += `<category><![CDATA[${tag}]]></category>`)
    );
    postsXml += `<item>
    <title>${article.content.title}</title>
    <link>https://budherd.com.au/articles/${article.slug}</link>
    <guid>https://budherd.com.au/articles/${article.slug}</guid>
    <pubDate>${DateTime.fromISO(article.first_published_at).toFormat(
      'ccc, dd LLL yyyy HH:mm:ss ZZ'
    )}</pubDate>
    <dc:creator>BudHerd</dc:creator>
    <description><![CDATA[
    ${article.content.seoDescription}
    ]]></description>
    <enclosure url="https:${
      article.content.mainImage
    }" length="1000" type="image/jpeg" />
    ${tagList}
    </item>`;
  });
  return postsXml;
};

const feedRss = (articles) => {
  const postsXml = articleXml(articles);
  return `<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
  <title>BudHerd</title>
  <link>https://budherd.com.au/feed</link>
  <description>
  Curated cannabis articles and information for Australians
  </description>
  <category>Cannabis/Marijuana/Medicinal Cannabis/Australia</category>
  <language>en-au</language>
    ${postsXml}
  </channel>
  </rss>`;
};

const Sitemap = () => {};

Sitemap.getInitialProps = async ({ res }) => {
  const articles = await fetchPosts();
  res.setHeader('Content-Type', 'application/rss+xml');
  res.write(feedRss(articles));
  res.end();
};

export default Sitemap;
