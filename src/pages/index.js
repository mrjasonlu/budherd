import HomeContainer from '~/containers/home';
import { fetchPosts, fetchStrains } from '~/resources';

const Home = (data = {}) => (
  <HomeContainer posts={data.posts} strains={data.strains} />
);
Home.getInitialProps = async function () {
  const posts = await fetchPosts();
  const strains = await fetchStrains({ _limit: 6 });
  return { posts, strains };
};
export default Home;
