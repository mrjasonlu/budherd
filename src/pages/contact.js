import {useState} from 'react';
import PageLayout from '~/components/layout';
import {NextSeo} from 'next-seo';
import {submitContact} from '~/resources';
import {
  Row,
  Col,
  Layout,
  Form,
  Input,
  Button
} from 'antd';
import ReCAPTCHA from 'react-google-recaptcha';

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not validate email!',
    number: '${label} is not a validate number!'
  },
  number: {
    range: '${label} must be between ${min} and ${max}'
  }
};

const layout = {
  layout: 'vertical',
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 24
  }
};

const {Content} = Layout;
export default({posts}) => {
  const recaptchaRef = React.createRef();
  const [captchaValue,
    setCaptchaValue] = useState('');
  const [captchaError,
    setCaptchaError] = useState(false);
  const [isLoading,
    setIsLoading] = useState(false);
  const [formSubmitted,
    setformSubmitted] = useState(false);
  const [formError,
    setFormError] = useState(false);

  const onChange = (value) => {
    setCaptchaValue(value);
    setCaptchaError(false);
  };

  const onSubmit = async(data) => {
    if (!captchaValue) {
      setCaptchaError(true);
      return;
    } else {
      try {
        setIsLoading(true);
        const success = await submitContact(data.user, captchaValue);
        setIsLoading(false);
        setformSubmitted(true);
      } catch (e) {
        setFormError(true);
        setIsLoading(false);
      }
    }
  };

  const contactForm = (
    <Form
      {...layout}
      name="nest-messages"
      onFinish={onSubmit}
      className="bui-padding-horizontal-large"
      validateMessages={validateMessages}>
      <Form.Item name={['user', 'name']} label="Name">
        <Input disabled={isLoading}/>
      </Form.Item>
      <Form.Item
        name={['user', 'email']}
        label="Email"
        rules={[{
          type: 'email',
          required: true
        }
      ]}>
        <Input disabled={isLoading}/>
      </Form.Item>
      <Form.Item
        name={['user', 'message']}
        label="Message"
        rules={[{
          required: true
        }
      ]}>
        <Input.TextArea disabled={isLoading} rows={5}/>
      </Form.Item>
      <div className="bui-margin-bottom">
        <ReCAPTCHA
          ref={recaptchaRef}
          sitekey="6LekJbAZAAAAAECRbaCj97QTyPo1fDJbjK9BkFfu"
          onChange={onChange}/> {captchaError
          ? (
            <div className="validation">
              <div>Captcha is required</div>
            </div>
          )
          : null}
      </div>
      <Form.Item wrapperCol={{
        ...layout.wrapperCol
      }}>
        <Button type="primary" block htmlType="submit" loading={isLoading}>
          Submit
        </Button>
      </Form.Item>
    </Form>
  );

  const thanksContent = (
    <div>
      <h1 className="bui-color-green">Thanks for the message</h1>
      <p>
        Thank you for contacting us! we will be in touch as soon as we can.
      </p>
    </div>
  );

  const errorContent = (
    <div className="bui-text-center">
      <h2 className="bui-color-green bui-smaller-font">
        Something went wrong.
      </h2>
      <p>
        Sorry, there was an error with sending the message. Please refresh the page and
        try again.{' '}
      </p>
      <p>
        Alternatively - please email us at{' '}
        <b className="bui-color-green">contact[at]budherd.com.au
        </b>
        and we will get back to you as soon as we can.
      </p>
    </div>
  );

  const mainContent = formError
    ? (errorContent)
    : (
      <div>
        <h1>Contact Us</h1>
        <p>
          Shoot us a message, question or give us some feedback.We will be in touch soon,
          we promise !
        </p>
        <div>
          {contactForm}
        </div>
      </div>
    );

  return (
    <PageLayout>
      <Row
        className="article-wrapper contact-form"
        justify="space-around"
        type="flex">
        <Col xs={22} sm={22} md={20} lg={12}>
          {formSubmitted
            ? thanksContent
            : mainContent}
        </Col>
      </Row>
      <NextSeo
        title="Contact | BudHerd"
        description="Contact us"
        canonical="https://budherd.com.au/contact"
        openGraph={{
        url: 'https://budherd.com.au/contact',
        title: 'Contact | BudHerd',
        description: 'Contact us'
      }}/>
    </PageLayout>
  );
};
