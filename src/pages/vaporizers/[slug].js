import { fetchPost } from '~/resources';
import Post from '~/containers/post';

const Article = ({ content, slug }) => {
  return <Post data={content} slug={slug} type="vape" />;
};

Article.getInitialProps = async function (context) {
  const { slug } = context.query;
  const content = await fetchPost(slug, 'vape');
  return { content, slug };
};

export default Article;
