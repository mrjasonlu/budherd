import { fetchPostsWithTags, fetchPosts } from '~/resources';
import VapeContainer from '~/containers/vaporizers';

const Articles = (data = {}) => {
  return <VapeContainer posts={data.posts} vapes={data.vapes} />;
};

Articles.getInitialProps = async function () {
  const posts = await fetchPostsWithTags(['Vaping'], '');
  const vapes = await fetchPosts({ starts_with: 'vape/' });
  return { posts, vapes };
};

export default Articles;
