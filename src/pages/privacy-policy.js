import { Row, Col, Layout } from 'antd';
import PageLayout from '~/components/layout';
import { NextSeo } from 'next-seo';

const { Content } = Layout;
export default () => {
  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <Col xs={24} sm={24} md={22} lg={20} xl={18} xxl={16}>
          <Content className="article-body">
            <h1 className="heading-h1">Privacy Policy</h1>
            <p>Last updated: March 8, 2020</p>

            <p>
              BudHerd ("us", "we", or "our") operates the{' '}
              <a href="https://budherd.com.au">
                https://budherd.com.au
              </a>{' '}
              website (the "Service"). BudHerd respect your privacy
              and are committed to protecting it through our
              compliance with this policy. This page informs you of
              our policies regarding the collection, use and
              disclosure of Personal Information when you use our
              Service.
            </p>
            <p>
              We will not use or share your information with anyone
              except as described in this Privacy Policy.
            </p>
            <p>
              We use your Personal Information for providing and
              improving the Service. By using the Service, you agree
              to the collection and use of information in accordance
              with this policy. Unless otherwise defined in this
              Privacy Policy, terms used in this Privacy Policy have
              the same meanings as in our Terms and Conditions,
              accessible at{' '}
              <a href="https://budherd.com.au">
                https://budherd.com.au
              </a>
            </p>
            <h2>Information Collection And Use</h2>
            <p>
              While using our Service, we may ask you to provide us
              with certain personally identifiable information that
              can be used to contact or identify you. Personally
              identifiable information may include, but is not limited
              to, your name, your email address ("Personal
              Information"). The purpose for which we collect personal
              information is to provide you with the best service
              experience possible on the Service and for our internal
              business purposes that form part of normal business
              practices. Some provision of personal information is
              optional. However, if you do not provide us with certain
              types of personal information, you may be unable to
              enjoy the full functionality of the Service.
            </p>

            <h2>Log Data</h2>
            <p>
              We collect information that your browser sends whenever
              you visit our Service ("Log Data"). This Log Data may
              include information such as your computer's Internet
              Protocol ("IP") address, browser type, browser version,
              the pages of our Service that you visit, the time and
              date of your visit, the time spent on those pages and
              other statistics. In addition, we may use third party
              services such as Google Analytics that collect, monitor
              and analyze this type of information in order to
              increase our Service's functionality. These third party
              service providers have their own privacy policies
              addressing how they use such information.
            </p>
            <h2>Cookies</h2>
            <p>
              Cookies are files with small amount of data, which may
              include an anonymous unique identifier. Cookies are sent
              to your browser from a web site and stored on your
              computer’s hard drive. We use “cookies” to collect
              information. You can instruct your browser to refuse all
              cookies or to indicate when a cookie is being sent.
              However, if you do not accept cookies, you may not be
              able to use some portions of our Service. We send a
              session cookie to your computer when you log in to your
              User account. This type of cookie helps if you visit
              multiple pages on the Service during the same session,
              so that you don’t need to enter your password on each
              page. Once you log out or close your browser, this
              cookie expires. We also use longer-lasting cookies for
              other purposes such as to display your Content and
              account information. We encode our cookie so that only
              we can interpret the information stored in them. Users
              always have the option of disabling cookies via their
              browser preferences. If you disable cookies on your
              browser, please note that some parts of our Service may
              not function as effectively or may be considerably
              slower.
            </p>

            <h2>Do Not Track Disclosure</h2>

            <p>
              We do not support Do Not Track ("DNT"). Do Not Track is
              a preference you can set in your web browser to inform
              websites that you do not want to be tracked. You can
              enable or disable Do Not Track by visiting the
              Preferences or Settings page of your web browser.
            </p>

            <h2>Service Providers</h2>
            <p>
              We may employ third party companies and individuals to
              facilitate our Service, to provide the Service on our
              behalf, to perform Service-related services or to assist
              us in analyzing how our Service is used. These third
              parties have access to your Personal Information only to
              perform these tasks on our behalf and are obligated not
              to disclose or use it for any other purpose.
            </p>

            <h2>Communications</h2>
            <p>
              We may use your Personal Information to contact you with
              newsletters, marketing or promotional materials and
              other information that may be of interest to you. You
              may opt out of receiving any, or all, of these
              communications from us by following the unsubscribe link
              or instructions provided in any email we send.
            </p>

            <h2>Business Transaction</h2>
            <p>
              In the event that we sell or buy businesses or their
              assets, or engage in transfers, acquisitions, mergers,
              restructurings, changes of control and other similar
              transactions, customer or user information is generally
              one of the transferable business assets. Thus, your
              personal information may be subject to such a transfer.
              In the unlikely event of insolvency, personal
              information may be transferred to a trustee or debtor in
              possession and then to a subsequent purchaser.
            </p>

            <h2>Security</h2>
            <p>
              The security of your Personal Information is important
              to us, and we strive to implement and maintain
              reasonable, commercially acceptable security procedures
              and practices appropriate to the nature of the
              information we store, in order to protect it from
              unauthorized access, destruction, use, modification, or
              disclosure. However, please be aware that no method of
              transmission over the Internet, or method of electronic
              storage is 100% secure and we are unable to guarantee
              the absolute security of the Personal Information we
              have collected from you.
            </p>

            <h2>International Transfer</h2>
            <p>
              Your information, including Personal Information, may be
              transferred to — and maintained on — computers located
              outside of your state, province, country or other
              governmental jurisdiction where the data protection laws
              may differ than those from your jurisdiction. If you are
              located outside Australia and choose to provide
              information to us, please note that we transfer the
              information, including Personal Information, to
              Australia and process it there. Your consent to this
              Privacy Policy followed by your submission of such
              information represents your agreement to that transfer.
            </p>

            <h2>Access and Correction</h2>
            <p>
              Australian Privacy Principle 6 of the Privacy Act 1988
              (Cth) allows you to get access to, and correct, the
              personal information we hold about you in certain
              circumstances. If you would like to obtain such access,
              please contact us on the details set out above. Please
              note that the access and correction requirements under
              this Privacy Policy operates alongside and do not
              replace other informal or legal procedures by which an
              individual can be provided access to, or correction of,
              their personal information, including the requirements
              under the Freedom of Information Act 1982 (Cth).
            </p>

            <h2>Complaints</h2>
            <p>
              Australian Privacy Principle 1 of the Privacy Act 1988
              (Cth) allows you to make a complaint about any alleged
              breaches of privacy. In order to lodge a complaint with
              us, please contact us using the details above with the
              following information: Your name and address; Details of
              the alleged breach of privacy; and URL link to the
              alleged breach of privacy (if applicable). Please allow
              us 30 days to investigate your complaint, after which we
              will contact you immediately to resolve the issue.
            </p>

            <h2>Retention of Information</h2>
            <p>
              We retain information for as long as required, allowed
              or we believe it useful, but do not undertake retention
              obligations. We may dispose of information in our
              discretion without notice, subject to applicable law
              that specifically requires the handling or retention of
              information. You must keep your own, separate back-up
              records.
            </p>

            <h2>Links To Other Sites</h2>
            <p>
              Our Service may contain links to other sites that are
              not operated by us. If you click on a third party link,
              you will be directed to that third party's site. We
              strongly advise you to review the Privacy Policy of
              every site you visit. We have no control over, and
              assume no responsibility for the content, privacy
              policies or practices of any third party sites or
              services.
            </p>

            <h2>Children's Privacy</h2>
            <p>
              Only persons age 18 or older have permission to access
              our Service. Our Service does not address anyone under
              the age of 13 ("Children"). We do not knowingly collect
              personally identifiable information from children under
              13. If you are a parent or guardian and you are aware
              that your Children has provided us with Personal
              Information, please contact us. If we discover that a
              Children under 13 has provided us with Personal
              Information, we will delete such information from our
              servers immediately.
            </p>

            <h2>Changes To This Privacy Policy</h2>
            <p>
              We may update our Privacy Policy from time to time. We
              will notify you of any changes by posting the new
              Privacy Policy on this page. You are advised to review
              this Privacy Policy periodically for any changes.
              Changes to this Privacy Policy are effective when they
              are posted on this page. If we make any material changes
              to this Privacy Policy, we will notify you either
              through the email address you have provided us, or by
              placing a prominent notice on our website.
            </p>
            <h2>Consent</h2>
            <p>
              You warrant that you are able to give consents under
              Australian Law or, in the event that you do not have the
              capacity to give consent, you warrant that your guardian
              or attorney is able to/ give any consent required under
              this Privacy Policy on your behalf. You hereby expressly
              and voluntarily grant your informed consent to us to
              deal with your personal information in accordance with
              the terms and conditions of this Privacy Policy. Should
              you retract your consent, please contact us. If you
              retract your consent, you acknowledge and agree that
              failure to provide certain types of personal information
              may not give you access to the full functionality of the
              Service.
            </p>

            <h2>Contact Us</h2>
            <p>
              If you have any questions about this Privacy Policy,
              please contact us at{' '}
              <a href="mailto:contact@budherd.com.au">
                contact@budherd.com.au
              </a>
            </p>
          </Content>
        </Col>
      </Row>
      <NextSeo
        title="Privacy Policy | BudHerd"
        description="BudHerd privacy policy"
        canonical="https://budherd.com.au/privacy-policy"
        openGraph={{
          url: 'https://budherd.com.au/privacy-policy',
          title: 'Privacy Policy | BudHerd',
          description: 'BudHerd privacy policy',
        }}
      />
    </PageLayout>
  );
};
