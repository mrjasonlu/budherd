import { Row, Col, Layout } from 'antd';
import PageLayout from '~/components/layout';
import { NextSeo } from 'next-seo';

const { Content } = Layout;
export default () => {
  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <Col xs={24} sm={24} md={22} lg={20} xl={18} xxl={16}>
          <Content className="article-body">
            <h1 className="heading-h1">Disclosure</h1>

            <p>
              Thank you for visiting the BudHerd website. As you may
              be aware, our foremost mission remains to educate
              Australians on the medicinal and recreational use of
              cannabis.
            </p>
            <p>
              To keep this site ad-free with increasing visitors and
              rising web hosting costs, budherd.com may use affiliate
              links within our articles. These links provide
              budherd.com.au with referral commissions when purchases
              are made through our links.
            </p>
            <p>
              We aim to keep our reviews and recommendations as our
              own, providing only unbiased opinions for our readers.
              Thanks again for visiting the BudHerd website and your
              support.
            </p>
          </Content>
        </Col>
      </Row>
      <NextSeo
        title="Disclosure | BudHerd"
        description="BudHerd Disclosure"
        canonical="https://budherd.com.au/disclosure"
        openGraph={{
          url: 'https://budherd.com.au/disclosure',
          title: 'Disclosure | BudHerd',
          description: 'BudHerd Disclosure',
        }}
      />
    </PageLayout>
  );
};
