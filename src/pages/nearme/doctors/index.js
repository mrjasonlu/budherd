import { Row, Col, Layout } from 'antd';
import PageLayout from '~/components/layout';
import { layoutSizes } from '~/config';
import { NextSeo } from 'next-seo';
import Breadcrumb from '~/components/breadcrumb';
import StateForm from '~/components/stateSelector';

const imageMain = require('~/assets/images/doctor.jpg?resize&sizes[]=300&sizes[]=600&sizes[]=800&sizes[]=10' +
  '00');

const { Content } = Layout;

export default () => {
  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <Breadcrumb
          crumbs={[
            {
              name: 'Doctors',
              path: '/nearme/doctors',
            },
          ]}
        />

        <div className="pageBanner landingBanner">
          <div className="bannerImage">
            <img
              alt="Cannabis field in the sunset"
              srcSet={imageMain.srcSet}
              src={imageMain.src}
              sizes="(min-width: 767px) 50vw, 100vw"
            />
          </div>
          <div className="bannerWrapper">
            <div className="bannerText">
              <h1>
                Find Experienced Medicinal Cannabis Doctors in
                Australia
              </h1>
              <p>
                Find a list of medical practitioners experienced with
                the TGA application process and medicinal cannabis
                products.
              </p>
              <StateForm path="/nearme/doctors/" small />
            </div>
          </div>
        </div>
        <Col xs={24} sm={24} md={19} lg={16} xl={13} xxl={12}>
          <Content className="article-body">
            <div className="bui-content bui-margin-top-xxlarge">
              <p>
                The first step for patients wanting access to
                medicinal cannabis involves a consultation with an
                experienced doctor. If deemed appropriate based on
                your medical history and condition, the doctor will
                then apply for approval through the Therapeutic Goods
                Administration (TGA) on your behalf.
              </p>
              <p>
                This process is somewhat convoluted and may be a bit
                overwhelming for many patients and doctors alike.
                Finding a doctor with an understanding of cannabis
                products and the TGA application process will ensure a
                smoother journey for both parties.
              </p>
              <p>
                Select your state or territory below to get started on
                finding an experienced cannabis medical practitioner
                in your area.
              </p>
            </div>
            <div>
              <h2 className="bui-text-center bui-color-green">
                Find Experienced Medicinal Cannabis Doctors
              </h2>
              <StateForm path="/nearme/doctors/" />
            </div>
            <div className="bui-padding-top-xxxlarge">
              <h2>Frequently Asked Questions</h2>
              <h2 className="bui-color-green">
                Is medical cannabis legal in Australia?
              </h2>
              <p>
                Yes, there are legal pathways for Australian patients
                seeking medicinal cannabis therapy. However, it's not
                as easy as getting a normal prescription from your GP.
                Most cannabis products are yet to be approved on the
                Australian Register of Therapeutic Goods, meaning
                doctors will require approval from the Therapeutic
                Goods Administration (TGA) prior to prescribing most
                cannabis products.
              </p>

              <h2 className="bui-color-green">
                Can any doctor prescribe cannabis?
              </h2>
              <p>
                In most Australian states and territories (with the
                exception of Tasmania), most qualified medical
                practitioners are able to prescribe medical cannabis
                products. Despite this, finding a doctor who is
                willing to prescribe medicinal cannabis may be an
                accomplishment in itself.
              </p>

              <h2 className="bui-color-green">
                Why won’t my doctor refer/prescribe cannabis products?
              </h2>

              <p>
                There is a severe lack of cannabis-related education
                for medical professionals. Only recently are doctors
                and clinics starting to open up on medicinal cannabis
                treatment options. Some common concerns for doctors
                include risks associated with mental health, negative
                side effects, lack of controlled trials and general
                stigma surrounding marijuana itself.
              </p>

              <p>
                Whilst medicinal cannabis has only recently become
                mainstream in Australia, there is already plenty of
                evidence supporting the therapeutic benefits of
                cannabis. But without large scale randomised control
                trials the majority of specialists will always be
                sceptical.
              </p>

              <p>
                Due to the reasons above, coupled with the involved
                TGA application process - many doctors are yet to be
                familiarised with medicinal cannabis.
              </p>
              <h2 className="bui-color-green">
                What conditions can medicinal cannabis be prescribed
                for?
              </h2>
              <p>
                The TGA website does not restrict the approval of
                medicinal cannabis to specific medical conditions,
                provided your doctor has the appropriate knowledge on
                product and the condition being treated.
              </p>
              <p>
                Some medical conditions that have been{' '}
                <a href="https://www.tga.gov.au/">
                  approved by the TGA
                </a>{' '}
                include (but not limited to):
              </p>
              <ul>
                <li>Chronic pain</li>
                <li>Anxiety and depression</li>
                <li>Epilepsy</li>
                <li>Schizophrenia</li>
                <li>Insomnia</li>
                <li>Nausea and vomiting</li>
                <li>Inflammation</li>
                <li>Palliative care indications</li>
                <li>Neuropathic pain</li>
                <li>Spasticity from neurological conditions</li>
                <li>Multiple sclerosis</li>
                <li>Fibromyalgia</li>
                <li>Arthritis</li>
                <li>Alzheimer’s</li>
                <li>Parkinson’s disease</li>
                <li>ADHD</li>
              </ul>

              <h2 className="bui-color-green">
                What types of cannabis products can I be prescribed?
              </h2>
              <p>
                Currently, only inhalation or orally administers
                products are available in Australia. The most commonly
                prescribed products are dried cannabis flower or
                cannabinoid oils such as CBD oil / THC oil.
                Cannabinoid oils can either be isolates,
                broad-spectrum or full-spectrum and are usually
                applied under the tongue in a tincture form.
                <br />
                Using a{' '}
                <a href="https://budherd.com.au/articles/what-is-vaping-and-how-to-choose-a-cannabis-vaporizer">
                  vaporizer
                </a>{' '}
                is the preferred method for consuming dried cannabis
                flowers.
              </p>

              <h2 className="bui-color-green">
                Where can I find out more information on accessing
                medicinal cannabis?
              </h2>
              <p>
                The TGA is Australia's regulatory authority for
                therapeutic goods, including medicinal cannabis
                products. You will find most of the information on
                their{' '}
                <a
                  href="https://www.tga.gov.au/access-medicinal-cannabis-products-1"
                  target="_blank">
                  access to medicinal cannabis products
                </a>{' '}
                page.
              </p>
              <p>
                Feel free to check out a list of topics we’ve covered
                in the{' '}
                <a href="https://budherd.com.au/articles">
                  cannabis education
                </a>{' '}
                section of our website:
              </p>
              <ul>
                <li>
                  <a href="https://budherd.com.au/articles/how-to-access-cbd-medical-marijuana-in-australia">
                    How to access medical marijuana in Australia
                  </a>
                </li>
                <li>
                  <a href="https://budherd.com.au/articles/medicincal-cannabis-faq-with-dr-steve-chalk">
                    Medicinal cannabis FAQ with Dr Stephen Chalk
                  </a>
                </li>
                <li>
                  <a href="https://budherd.com.au/articles/complete-guide-to-cbd-oil-in-australia">
                    Complete guide to CBD oil in Australia
                  </a>
                </li>
                <li>
                  <a href="https://budherd.com.au/articles/thc-and-cbd-overview-for-australians">
                    CBD vs. THC
                  </a>
                </li>
              </ul>
            </div>
          </Content>
        </Col>
      </Row>
      <NextSeo
        title="Experienced Medicinal Cannabis Doctors near me | BudHerd"
        description="Find doctors and cannabis clinics experienced with TGA application and medicinal cannabis products in Australia."
        canonical="https://budherd.com.au/nearme/doctors"
        openGraph={{
          url: 'https://budherd.com.au/nearme/doctors',
          title:
            'Experienced Medicinal Cannabis Doctors near me | BudHerd',
          description:
            'Find doctors and cannabis clinics experienced with TGA application and medicinal cannabis products in Australia.',
        }}
      />
    </PageLayout>
  );
};
