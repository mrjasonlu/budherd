import { fetchDoctors } from '~/resources';
import DoctorList from '~/containers/doctorList';

const Doctors = (data = {}) => {
  const { list, context, total, state } = data;
  return (
    <DoctorList
      list={list}
      context={context}
      total={total}
      state={state}
    />
  );
};

Doctors.getInitialProps = async function (context) {
  const { query, pathname } = context;
  const { _page = 1, _sort, _order, state, category } = query || {};
  const res = await fetchDoctors({
    _page,
    _sort,
    _order,
    state,
    category,
  });
  const { data, total } = res;
  return {
    list: data,
    context: {
      query,
      pathname,
    },
    total,
    state,
  };
};

export default Doctors;
