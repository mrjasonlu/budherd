import { Row, Col, Layout } from 'antd';
import PageLayout from '~/components/layout';
import { NextSeo } from 'next-seo';

const { Content } = Layout;
export default () => {
  return (
    <PageLayout>
      <Row
        className="article-wrapper"
        justify="space-around"
        type="flex">
        <Col xs={24} sm={24} md={22} lg={20} xl={18} xxl={16}>
          <Content className="article-body">
            <h1 className="heading-h1">Terms of Use</h1>
            <p>Last updated: March 8, 2020</p>

            <p>
              Please read these Terms and Conditions ("Terms", "Terms
              and Conditions") carefully before using the{' '}
              <a href="https://budherd.com.au">
                https://budherd.com.au
              </a>{' '}
              website (the "Service") operated by BudHerd ("us", "we",
              or "our"). Your access to and use of the Service is
              conditioned on your acceptance of and compliance with
              these Terms. These Terms apply to all visitors, users
              and others who access or use the Service. By accessing
              or using the Service you agree to be bound by these
              Terms. If you disagree with any part of the terms then
              you may not access the Service.
            </p>

            <h2>Communications</h2>
            <p>
              By creating an Account on our service, you agree to
              subscribe to newsletters, marketing or promotional
              materials and other information we may send. However,
              you may opt out of receiving any, or all, of these
              communications from us by following the unsubscribe link
              or instructions provided in any email we send.
            </p>

            <h2>Purchases</h2>
            <p>
              If you wish to purchase any product or service made
              available through the Service ("Purchase"), you may be
              asked to supply certain information relevant to your
              Purchase including, without limitation, your credit card
              number, the expiration date of your credit card, your
              billing address, and your shipping information. You
              represent and warrant that: (i) you have the legal right
              to use any credit card(s) or other payment method(s) in
              connection with any Purchase; and that (ii) the
              information you supply to us is true, correct and
              complete. By submitting such information, you grant us
              the right to provide the information to third parties
              for purposes of facilitating the completion of
              Purchases. We reserve the right to refuse or cancel your
              order at any time for certain reasons including but not
              limited to: product or service availability, errors in
              the description or price of the product or service,
              error in your order or other reasons. We reserve the
              right to refuse or cancel your order if fraud or an
              unauthorised or illegal transaction is suspected. The
              Service is not designed to provide qualitative advice
              regarding the fitness for purpose and merchantability of
              any products or services. We strongly advise you to
              exercise caution and do your own due diligence in
              conducting any transactions through the Service.
            </p>

            {/* <h2>Accounts</h2>
            <p>
              When you create an account with us, you guarantee that
              you are above the age of 18, and that the information
              you provide us is accurate, complete, and current at all
              times. Inaccurate, incomplete, or obsolete information
              may result in the immediate termination of your account
              on the Service. You are responsible for maintaining the
              confidentiality of your account and password, including
              but not limited to the restriction of access to your
              computer and/or account. You agree to accept
              responsibility for any and all activities or actions
              that occur under your account and/or password, whether
              your password is with our Service or a third-party
              service. You must notify us immediately upon becoming
              aware of any breach of security or unauthorized use of
              your account. You agree not to disclose your password to
              any third party. You must notify us immediately upon
              becoming aware of any breach of security or unauthorized
              use of your account. We reserve the right to refuse
              service, terminate accounts, remove or edit content in
              our sole discretion.
            </p> */}

            <h2>Intellectual Property</h2>

            <p>
              The Service and its original content (excluding Content
              provided by users), features and functionality are and
              will remain the exclusive property of BudHerd and its
              licensors. The Service is protected by copyright,
              trademark, and other laws of both the Australia and
              foreign countries. Our trademarks and trade dress may
              not be used in connection with any product or service
              without the prior written consent of BudHerd. Nothing in
              these Terms constitutes a transfer of any Intellectual
              Property rights from us to you.
            </p>

            <h2>Links To Other Web Sites</h2>
            <p>
              Our Service may contain links to third-party web sites
              or services that are not owned or controlled by BudHerd.
              BudHerd has no control over, and assumes no
              responsibility for, the content, privacy policies, or
              practices of any third party web sites or services. You
              further acknowledge and agree that BudHerd shall not be
              responsible or liable, directly or indirectly, for any
              damage or loss caused or alleged to be caused by or in
              connection with use of or reliance on any such content,
              goods or services available on or through any such web
              sites or services. We strongly advise you to read the
              terms and conditions and privacy policies of any
              third-party web sites or services that you visit.
            </p>

            {/* <h2>Termination</h2>
            <p>
              We may terminate or suspend your account immediately,
              without prior notice or liability, for any reason
              whatsoever, including without limitation if you breach
              the Terms. Upon termination, your right to use the
              Service will immediately cease. If you wish to terminate
              your account, you may simply discontinue using the
              Service. All provisions of the Terms which by their
              nature should survive termination shall survive
              termination, including, without limitation, ownership
              provisions, warranty disclaimers, indemnity and
              limitations of liability. We shall not be liable to you
              or any third party for any claims or damages arising out
              of any termination or suspension or any other actions
              taken by us in connection therewith. If applicable law
              requires us to provide notice of termination or
              cancellation, we may give prior or subsequent notice by
              posting it on the Service or by sending a communication
              to any address (email or otherwise) that we have for you
              in our records.
            </p> */}

            <h2>Indemnification</h2>
            <p>
              You agree to defend, indemnify and hold harmless BudHerd
              and its licensee and licensors, and their employees,
              contractors, agents, officers and directors, from and
              against any and all claims, damages, obligations,
              losses, liabilities, costs or debt, and expenses
              (including but not limited to legal fees and expenses),
              resulting from or arising out of a) your use and access
              of the Service, by you or any person using your account
              and password; b) a breach of these Terms, or c) Content
              posted on the Service. This indemnification section
              survives the expiration of your registration, and
              applies to claims arising both before and after the
              registration ends.
            </p>

            <h2>Limitation Of Liability</h2>
            <p>
              In no event shall BudHerd, nor its directors, employees,
              partners, agents, suppliers, or affiliates, be liable
              for any indirect, incidental, special, consequential or
              punitive damages, including without limitation, loss of
              profits, data, use, goodwill, or other intangible
              losses, resulting from (i) your access to or use of or
              inability to access or use the Service; (ii) any conduct
              or content of any third party on the Service; (iii) any
              content obtained from the Service; and (iv) unauthorized
              access, use or alteration of your transmissions or
              content, whether based on warranty, contract, tort
              (including negligence) or any other legal theory,
              whether or not we have been informed of the possibility
              of such damage, and even if a remedy set forth herein is
              found to have failed of its essential purpose.
            </p>

            <h2>Disclaimer</h2>
            <p>
              Your use of the Service is at your sole risk. The
              Service is provided on an "AS IS" and "AS AVAILABLE"
              basis. The Service is provided without warranties of any
              kind, whether express or implied, including, but not
              limited to, implied warranties of merchantability,
              fitness for a particular purpose, non-infringement or
              course of performance. BudHerd its subsidiaries,
              affiliates, and its licensors do not warrant that a) the
              Service will function uninterrupted, secure or available
              at any particular time or location; b) any errors or
              defects will be corrected; c) the Service is free of
              viruses or other harmful components; or d) the results
              of using the Service will meet your requirements. This
              disclaimer of liability applies to any damages or injury
              caused by any failure of performance, error, omission,
              interruption, deletion, defect, delay in operation or
              transmission, computer virus, communication line
              failure, theft, or destruction or unauthorized access
              or, alteration of or use of record in connection with
              the use or operation of the Service, whether for breach
              of contract, tortious behaviour, negligence or any other
              cause of action. We make no representations or
              warranties of any kind, express or implied, about the
              completeness, accuracy, reliability, suitability or
              availability with respect to the content contained on
              the Service for any purpose. Any reliance you place on
              such information is therefore strictly at your own risk.
              We disclaim any express or implied warranty
              representation or guarantee as to the effectiveness or
              profitability of the Service or that the operation of
              our Service will be uninterrupted or error-free. We are
              not liable for the consequences of any interruptions or
              error in the Service.
            </p>

            <h2>Exclusions</h2>
            <p>
              Some jurisdictions do not allow the exclusion of certain
              warranties or the exclusion or limitation of liability
              for consequential or incidental damages, so the
              limitations above may not apply to you.
            </p>

            <h2>Governing Law</h2>
            <p>
              These Terms shall be governed and construed in
              accordance with the laws of Victoria, Australia, without
              regard to its conflict of law provisions. Our failure to
              enforce any right or provision of these Terms will not
              be considered a waiver of those rights. If any provision
              of these Terms is held to be invalid or unenforceable by
              a court, the remaining provisions of these Terms will
              remain in effect. These Terms constitute the entire
              agreement between us regarding our Service, and
              supersede and replace any prior agreements we might have
              between us regarding the Service.
            </p>

            <h2>Changes</h2>
            <p>
              We reserve the right, at our sole discretion, to modify
              or replace these Terms at any time. If a revision is
              material we will try to provide at least 30 days notice
              prior to any new terms taking effect. What constitutes a
              material change will be determined at our sole
              discretion. It is your sole responsibility to
              periodically check these Terms for any changes. If you
              do not agree with any of the changes to these Terms, it
              is your sole responsibility to stop using the Service.
              Your continued use of the Service will be deemed as your
              acceptance thereof.
            </p>

            <h2>Contact Us</h2>
            <p>
              If you have any questions about these Terms, please
              contact us at{' '}
              <a href="mailto:contact@budherd.com.au">
                contact@budherd.com.au
              </a>
            </p>
          </Content>
        </Col>
      </Row>
      <NextSeo
        title="Terms Of Use | BudHerd"
        description="BudHerd terms of use"
        canonical="https://budherd.com.au/terms-of-use"
        openGraph={{
          url: 'https://budherd.com.au/terms-of-use',
          title: 'Terms Of Use | BudHerd',
          description: 'BudHerd terms of use',
        }}
      />
    </PageLayout>
  );
};
