const withNextAntdLess = require('./next-antd-less.config');
const withPlugins = require('next-compose-plugins');
const optimizedImages = require('next-optimized-images');
const lessToJS = require('less-vars-to-js');
const path = require('path');
const fs = require('fs');
const themeVariables = lessToJS(
  fs.readFileSync(
    path.resolve(__dirname, './src/assets/antd-custom.less'),
    'utf8'
  )
);

module.exports = withPlugins(
  [
    [
      withNextAntdLess,
      {
        cssModules: true,
        cssLoaderOptions: {
          importLoaders: 1,
          localIdentName: '[local]___[hash:base64:5]',
        },
        lessLoaderOptions: {
          javascriptEnabled: true,
          modifyVars: themeVariables,
        },
      },
    ],
    [
      optimizedImages,
      {
        inlineImageLimit: 8192,
        imagesFolder: 'images',
        imagesName: '[name]-[hash].[ext]',
        handleImages: ['jpeg', 'png'],
        optimizeImages: true,
        optimizeImagesInDev: false,
        mozjpeg: {
          quality: 80,
        },
        optipng: {
          optimizationLevel: 3,
        },
      },
    ],
  ],
  {
    generateBuildId: async () => {
      if (process.env.BUILD_ID) {
        return process.env.BUILD_ID;
      } else {
        return `${new Date().getTime()}`;
      }
    },
    env: {
      API_TOKEN: process.env.STORYBLOK_API_TOKEN,
      API_TOKEN_PREVIEW: process.env.STORYBLOK_API_TOKEN_PREVIEW,
      MONGO_PASS: process.env.MONGO_PASS,
      MONGO_USER: process.env.MONGO_USER,
      CAPTCHA: process.env.CAPTCHA,
      SENDGRID: process.env.SENDGRID,
      MAP_API_KEY: process.env.MAP_API_KEY,
    },
  }
);
